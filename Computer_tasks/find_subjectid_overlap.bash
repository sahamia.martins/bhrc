#!/usr/bin/env bash

# Bash script to find the subjectid overlaps in files after E-MERGE
# Run the following command in bash to make the script executable:
# chmod u+x find_subjectid_overlap.bash

## List of files not merged in e-merge
### RS
#R1-A-GNG-11435 ok
#R1-H-S400-10749-2.edat2 ok
#R1-K-DOT-S-10565-2.edat2 ok
#R1-K-DOT-S-10737-2.edat2 ok
#R1-K-DOT-S-11272-2.edat2 ok

### SP
#R1-A-GNG-21332-2.edat2 ok
#R1-A-GNG-21646-2.edat2 ok
#R1-C-MSTR-20753-2.edat2 ok
#R1-C-MSTR-21332-2.edat2 ok
#R1-G-2CR-21332-2.edat2 ok
#R1-G-2CR-21428-2.edat2 ok
#R1-H-S400-21332-2.edat2 ok
#R1-H-S400-21371-2.edat2 ok
#R1-K-DOT-S-21332-2.edat2 ok

## set variables with path names before run the script
ORIGIN_RS=~/Documentos/BHRC/W2/Ordered/RS
ORIGIN_SP=~/Documentos/BHRC/W2/Ordered/SP

## convert from UTF-16LE to utf-8
find . -type f -name '*.txt' -print -exec iconv -f UTF-16LE -t utf-8 {} -o {}.utf8 \;

## RS
### A-GNG
cd $ORIGIN_RS/H-S400
awk '/Subject: 10749/ {print "RS" FS FILENAME " -> " $0}' *.utf8
cd $ORIGIN_RS/K-DOTS
awk '/Subject: 10565/ || /Subject: 10737/ || /Subject: 11272/ {print "RS" FS FILENAME " -> " $0}' *.utf8

## SP
cd $ORIGIN_SP/A-GNG
awk '/Subject: 21332/ || /Subject: 21646/ {print "SP" FS FILENAME " -> " $0}' *.utf8
cd $ORIGIN_SP/C-MSTR
awk '/Subject: 20753/ || /Subject: 21332/ {print "SP" FS FILENAME " -> " $0}' *.utf8
cd $ORIGIN_SP/G-2CR
awk '/Subject: 21332/ || /Subject: 21428/ {print "SP" FS FILENAME " -> " $0}' *.utf8
cd $ORIGIN_SP/H-S400
awk '/Subject: 21332/ || /Subject: 21371/ {print "SP" FS FILENAME " -> " $0}' *.utf8
cd $ORIGIN_SP/K-DOTS
awk '/Subject: 21332/ {print "SP" FS FILENAME " -> " $0}' *.utf8

## remove utf8 files
cd $ORIGIN_RS
find . -type f -name '*.utf8' -print -exec rm {} \;
cd $ORIGIN_SP
find . -type f -name '*.utf8' -print -exec rm {} \;
