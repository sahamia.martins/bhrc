---
title: "INPD Go-NoGO (AGNG)"
author: "André Rafael Simioni"
date: "23 de junho de 2017"
output: html_document
editor_options: 
  chunk_output_type: console
---
# Librarys and settings
First, read the README.md section about `settings.r` file.
```{r lib,  include=FALSE}
# Aditional librarys
library(lubridate)

# Load settings.r
setwd("/home/andresimi/Documentos/pCloud/Projetos/Analises/bhrc/")
source("settings.r", echo = T)
```

# Match with redcap?
Match with subjectids from redcap. Don't need to run.
```{r eval=FALSE, include=FALSE}
# Load data
subjectid <- import("bhrc_data/Proc/Dangerinfo.rds", setclass = "tibble") %>% select(-name_mother:-name_father)
agngW0_raw <- import("bhrc_data/Raw/neuropsychology/W0_raw/Aexp.sav",setclass="tibble")
agngW1_raw <- import("bhrc_data/Raw/neuropsychology/W1_raw/agng_w1_raw.rds",setclass="tibble")
agngW2_raw <- import("bhrc_data/Raw/neuropsychology/W2_raw/agng_w2_raw.rds",setclass="tibble")

# function
notmatch <- function(df_raw, id_var, Nome=NULL, n=10) {
  id_var <- enquo(id_var)
  df <- df_raw %>% transmute(subjectid = !! id_var, Nome)
  cat("Subjects that are on x but not on Redcap's subjectid \n")
  anti_join(df, subjectid) %>% distinct()  %>% print(n=n)
}

# not match
notmatch(agngW0_raw, subjectid)
notmatch(agngW1_raw, Subject)
notmatch(agngW2_raw, Subject)

#subjectid %>% filter(str_detect(name_child, "ARAUJO")) %>% print(n=100)
```

# Load data
```{r}
# Open data and settings
subjectid <- import("bhrc_data/Proc/subjectid.rds", setclass = "tibble")

agngW0_raw <- import("bhrc_data/Raw/neuropsychology/W0_raw/Aexp.sav", setclass = "tibble") %>% 
  transmute(subjectid, 
            redcap_event_name = "wave0_arm_1", 
            SessionDate = as.character(ADate), 
            SessionTime = as.character(Atime)%>% str_sub(1, 8), 
            SessionTimeUtc = as.character(SessionTimeUtc)%>% str_sub(1, 8),
            Trial, Target, Wait1.RespA, Wait1.RT)
agngW0_raw %>% sumfac(-1:-2)

agngW1_raw <- import("bhrc_data/Raw/neuropsychology/W1_raw/agng_w1_raw.rds", setclass = "tibble") %>% 
  transmute(subjectid = Subject, 
            redcap_event_name = "wave1_arm_1", 
            SessionDate = mdy(SessionDate) %>% as.character("%Y-%m-%d"),
            SessionTime, 
            SessionTimeUtc = as.character(SessionTimeUtc)%>% str_sub(1, 8),
            Trial, Target, Wait1.RespA, Wait1.RT)
agngW1_raw %>% sumfac(-1:-2)

agngW2_raw <- import("bhrc_data/Raw/neuropsychology/W2_raw/agng_w2_raw.rds", setclass = "tibble") %>% 
  transmute(subjectid = Subject, 
            redcap_event_name = "wave2_arm_1", 
            SessionDate = mdy(SessionDate) %>% as.character("%Y-%m-%d"),
            SessionTime, 
            SessionTimeUtc = as.character(SessionTimeUtc)%>% str_sub(1, 8),
            Trial, Target, Wait1.RespA, Wait1.RT,
            across(c(Wait1.RespA, Wait1.RT), ~as.numeric(.))
            )
agngW2_raw %>% sumfac(-1:-2)
  
# Differences between dataframes
setdiff(names(agngW0_raw), names(agngW1_raw)); setdiff(names(agngW1_raw), names(agngW0_raw))

# Bind rows
agng_raw  <- bind_rows(agngW0_raw, agngW1_raw, agngW2_raw)

# how many trials?
agng_raw$Trial %>% summary()
agng_raw %>% group_by(subjectid, redcap_event_name) %>% count() %>% summary()

# which ids with duplicated data?
agng_raw %>% group_by(subjectid, redcap_event_name) %>% count() %>% arrange(n) %>% tail()

# explore
agng_raw %>% filter(subjectid %in% c(10615, 10755, 21706, 11435))

# remove duplicated tests
agng_raw  <- agng_raw %>% 
  # subjectids(10615, 10755, 21706) performed 2 times the GNG on baseline. Select only the first time.
  mutate(SessionDate = ymd(SessionDate)) %>% 
  arrange(subjectid, redcap_event_name, Trial, SessionDate) %>% 
  group_by(redcap_event_name, subjectid, Trial) %>% 
  filter(row_number()==1) %>% 
  ungroup() %>% 
  mutate(SessionDate = as.character(SessionDate))

rm(agngW0_raw, agngW1_raw, agngW2_raw)
```
# Compute variables  
Variable levels:
- a_target=c(nogo=0, left=1, right=2), 
- a_resp=c(nogo=0, left=1, right=2), 
- *a_cor=c('correct go'=0, 'correct nogo'=1, 'comission error'=2, 'omission error'=3, 'Opposite direction error'=4)*

```{r}
agng_lng <- agng_raw %>%
  # Select,  rename and create variables
  transmute(subjectid, 
            redcap_event_name,
            a_date = SessionDate,
            a_time = SessionTime, 
            a_trial = as.integer(Trial),  # Trial number
            a_target = rec(Target, rec = "nogo.bmp=0; left_green.bmp=1; right_green.bmp=2"),
            a_resp = rec(Wait1.RespA, rec = "NA=0; else=copy"),  # recode NA as nogo
            a_cor = case_when(a_target==0 & a_resp!=0 ~ 2,  # comission error, press when shouldn't press*
                              a_target!=0 & a_resp==0 ~ 3, # omission error
                              a_target==1 & a_resp==2 ~ 4, # opp direction error
                              a_target==2 & a_resp==1 ~ 4, # opp direction error
                              a_target==1 & a_resp==1 ~ 0, # correct go
                              a_target==2 & a_resp==2 ~ 0, # correct go
                              a_target==0 & a_resp==0 ~ 1), # correct nogo
            a_rt = Wait1.RT, 
            a_cor_rt = if_else(a_cor==0, a_rt, NA_real_),  # RTs from correct GO trials
            a_err_rt = if_else(a_cor==2, a_rt, NA_real_)) %>% # RTs from COMISSION ERROR trials
  # Group dataframe by IDs and Remove the 10 training trials plus 2 initial trials (of a total of 110)
  group_by(subjectid, redcap_event_name)%>%
  arrange(subjectid, redcap_event_name, a_trial) %>% 
  filter(a_trial > 12)%>%
  # Compute % of correct/wrong answers
  mutate(a_go = sum(a_target!=0, na.rm=T),  # nº of go trials
         a_nogo = sum(a_target==0, na.rm=T),  # nº of nogo trials
         a_pcor = sum(a_cor<=1, na.rm=T)/n(),  # % correct trials
         a_pcor_nogo = sum(a_cor==1, na.rm=T)/a_nogo,  # % correct nogo trials
         a_perr = sum(a_cor>1, na.rm=T)/n(),  # % wrong trials
         a_pcom = sum(a_cor==2, na.rm=T)/a_nogo,  # % comission erros
         a_pomi = sum(a_cor==3, na.rm=T)/a_go,   # % omission erros
         a_popp = sum(a_cor==4, na.rm=T)/a_go,  # % opposite direction error
  # Define outliers for each subject
         a_mrt_cor = mean(a_cor_rt, na.rm=T),  # mean and sd RT from GO CORRECT trials PRE-outlier removal
         a_sdrt_cor = sd(a_cor_rt, na.rm=T), 
         a_out = if_else((a_cor_rt>0.1 & a_cor_rt<100) | a_cor_rt>a_mrt_cor+3*a_sdrt_cor, T, F),  # signal the outlier trials
         a_pout = sum(a_out, na.rm=T)/a_go,  # % outliers in GO Trials
         a_cor_rt = if_else((a_cor_rt>0.1 & a_cor_rt<100) | a_cor_rt>a_mrt_cor+3*a_sdrt_cor, NA_real_, a_cor_rt),  # Exclude Trials with (>3SD of the own subject) or impossible values (<100ms)
  # Post-error RT
         a_pert = if_else(a_cor>1 & lead(a_cor, order_by=a_trial)<=1, lead(a_cor_rt, order_by=a_trial), NA_real_), 
  # Summary statistics
         a_mrt_cor = mean(a_cor_rt, na.rm=T),  # mean RT from GO CORRECT trials POS-outlier removal
         a_sdrt_cor = sd(a_cor_rt, na.rm=T), 
         a_mrt_err = mean(a_err_rt, na.rm=T),  # mean RT from COMISSION ERRORS trials
         a_sdrt_err = sd(a_err_rt, na.rm=T), 
         a_mrt_pert = mean(a_pert, na.rm=T),  # mean post error RT
         a_sdrt_pert = sd(a_pert, na.rm=T), 
  # Filter variable with subjects with >65% error (n=108)
         a_ok = if_else(a_perr<0.65, T, F))%>%
  # Final Settings
  ungroup() %>%
  left_join(subjectid) %>%
  relocate(ident, .before=1) %>% 
  select(-a_go, -a_nogo, -a_out)
```

## Set labels and Wide dataframe
```{r}
# Long
agng_lng  <- agng_lng %>%
  var_labels(#a_Order = 'Go/No-Go: Order of the test', 
            a_date = 'SessionDate',
            a_time = 'SessionTime', 
            a_trial = 'Go/No-Go: Trial nº', 
            a_target = 'Go/No-Go: Target', 
            a_resp = 'Go/No-Go: Answer', 
            a_rt = 'Go/No-Go: Response time', 
            a_cor = 'Go/No-Go: type of answer', 
            a_cor_rt = 'Go/No-Go: response time of correct go trials', 
            a_err_rt = 'Go/No-Go: response time of comission error trials',
            a_pcor = 'Go/No-Go: % Correct',
            a_pcor_nogo = 'Go/No-Go: % Correct nogo', 
            a_perr = 'Go/No-Go: Error', 
            a_pcom = 'Go/No-Go: Comission Errors', 
            a_pomi = 'Go/No-Go: Omission Erors', 
            a_popp = 'Go/No-Go: Errors in the opposite direction', 
            a_ok = 'Go/No-Go: selecting >65% accuracy', 
            a_pout = 'Go/No-Go: % Outliers (RTs with <100ms OR >3SDs)', 
            a_mrt_cor = 'Go/No-Go: Mean RT of Correct Go trials', 
            a_sdrt_cor = 'Go/No-Go: SD RT of Correct Go trials', 
            a_mrt_err = 'Go/No-Go: Mean RT of Incorrect No-Go trials (comission errors)', 
            a_sdrt_err = 'Go/No-Go: SD RT of Incorrect No-Go trials (comission errors)', 
            a_pert = 'Go/No-Go: Post Error RT', 
            a_mrt_pert = 'Go/No-Go: Mean Post Error RT', 
            a_sdrt_pert = 'Go/No-Go: SD Post Error RT') %>% 
  set_labels(a_target, a_resp, labels = c(nogo = 0, left = 1, right = 2)) %>% 
  set_labels(a_cor, labels = c('correct go' = 0, 'correct nogo' = 1, 'comission error' = 2, 'omission error' = 3, 'Opposite direction error' = 4))

# Time series to wide dataframe
agng_lng %>% names()
agng_wide  <- agng_lng %>% 
  select(ident, subjectid, redcap_event_name, a_date, a_time, a_pcor:a_pout, -a_ok) %>% # remove time series variables
  distinct() %>%  # remove duplicate results
  copy_labels(agng_lng)

# Missings?
#miss_agngW0 <-agngW0_wide%>% filter(a_pomi>0.99)

# Prune long dataframe
#agng_lng  <- agng_lng%>% select(subjectid:a_err_rt, a_pcom, a_pert, a_ok)
```

# Find duplicates
```{r eval=FALSE,  include=FALSE}
agng_wide %>% filter(redcap_event_name=="wave0_arm_1") %>% filter(duplicated(subjectid))
agng_wide %>% filter(redcap_event_name=="wave0_arm_1") %>% filter(subjectid==10615 | subjectid==10755 | subjectid==21706)
```

# Export
```{r}
agng_lng %>% select(-subjectid) %>% export("bhrc_data/Proc/agng_lng.rds")
agng_wide %>% select(-subjectid) %>% export("bhrc_data/Proc/agng_wide.rds")
```
