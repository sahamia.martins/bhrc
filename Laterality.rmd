---
title: "HRC - Laterality"
author: "André Simioni"
date: "7 de novembro de 2018"
output: html_document
editor_options: 
  chunk_output_type: console
---

# Librarys and settings
First, read the README.md section about `settings.r` file.
```{r lib,  include=FALSE}
setwd("~/Documentos/pCloud/Projetos/Analises/bhrc")

# Aditional librarys
library(lubridate)

# Load settings.r
source("settings.r", echo = T)
```

# Get data
```{r}
latera_raw <- import(bhrc_noidraw, setclass = "tibble") %>%
  filter(redcap_event_name == "wave0_arm_1") %>%
  select(ident, redcap_event_name, starts_with("p_lat"), starts_with("p_oms"))
```

# Export
```{r}
latera_raw %>% export("bhrc_data/Proc/Laterality.rds")
```

