---
title: "BHRC Export data from form"
author: "André Simioni"
date: "21/09/2020"
output: html_document
editor_options: 
  chunk_output_type: console
---

# Librarys and settings
First, read the README.md section about `settings.r` file.
```{r lib,  include=FALSE}
# Aditional librarys
library(clipr); library(googlesheets4); library(zip); library(lubridate); library(blastula); library(glue)

# Load settings.r
setwd("~/Documentos/pCloud/Projetos/Analises/bhrc")
source("settings.r", echo = T)
```

# Update form
## Open dict
```{r}
# Open data
dic_raw <- import("Dictionarys/BHRC Dict of Variables - W0W1W2.xlsx", setclass = "tibble") %>% 
  transmute(variable = `Variable / Field Name`,
            form = `Form Name`) %>% 
  # Remove registry forms and not used ones
  filter(!(str_detect(form, "regis") | str_detect(form, "dom_s07_dawba_w0_w1") | str_detect(form, "psi_s01_dawba_w1")))
#dic_raw %>% names()
```

## Write to clip
```{r eval=FALSE, include=FALSE}
instruments <- dic_raw$form %>% get_labels()
instruments %>% length()

# copy to clipboard
instruments %>% write_clip()
```
paste instruments to the Google form

# Get requests from google
"\\S*_+\\S*"
```{r}
# Authenticate
gs4_auth(email = "andresimi@gmail.com")
prop_drive <- gs4_get("1MpIeNHxWx7wSNKvf_l6vVk2DO8BlXA6fKIob84-52fE")
prop_raw <- read_sheet(prop_drive, "form1") %>% 
  rename(email = `Endereço de e-mail`, date_time=`Carimbo de data/hora`) %>% 
  drop_na(date_time, email) %>% 
  mutate(date_time = unlist(date_time) %>% unlist %>% as_datetime)

# clean req
prop <- prop_raw %>% 
  rename_all(~str_extract(., "\\S*+") %>% str_remove_all("(\\[|\\])")) %>% 
  mutate(waves = str_extract_all(waves, "\\S*(Wave|_)+\\S*"),
         forms = str_split(forms, ", "),
         ext = str_extract(ext, "\\S*+"),
         across(c(starts_with("approved"), sent_data, sent_mail), ~str_extract(.,"\\d")),
         comitee_mail = case_when(str_detect(comitee, "Dawba") & str_detect(comitee, "Genetics") ~ "aryararipe@gmail.com, sinbelangero@gmail.com",
                                  str_detect(comitee, "Dawba") ~ "aryararipe@gmail.com",
                                  str_detect(comitee, "Genetics") ~ "sinbelangero@gmail.com",
                                  TRUE ~ NA_character_))
```

# Send requests for approval
## Filter
```{r}
prop_req1 <- prop %>% 
  filter(is.na(approved1) & is.na(approved2) & is.na(approved3) & is.na(approved4) & is.na(approved5) & sent_mail==0) # filter requests
  #filter(approved1 >=2 & approved1 >=2 & approved1 >=2 & approved1 >=2 & approved1 >=2 & sent !=1)  # filter requests

prop_req2 <- prop_req1 %>% 
  group_by(propid) %>% 
  group_split() %>% 
  set_names(str_c("p", prop_req1$propid))
```

## Email
### Make email
```{r}
make_email <- function(df) {
  email <- compose_email(
  header = glue("## BHRC Proposal ID: {df$propid}") %>% md,
  body = glue("
    ## Identification
    __Date time:__ {df$date_time}  
    __Proponent:__ {df$proponent}  
    __E-mail:__ {df$email}  
    __Proponent Institution:__ {df$institution}  
    __Other researchers involved in the proposal:__
    {df$other}  
    
    ## Proposal 
    __Title:__ {df$title}  
    
    __Rationale and background:__  {df$rationale}  
    
    __Hypothesis:__  {df$hypo}  
    
    __(P) Population or problem - please describe the eligibility criteria for participants:__  {df$picot_p}  
    
    __(I) Describe the intervention/exposure/issue of interest:__  {df$picot_i}  
    
    __(C) Comparison - what is the alternative for intervention/exposure/issue of interest?__  {df$picot_c}  
    
    __(O) What are the outcome you would like to study?__  {df$picot_o}
    
    __(T) Time for the exposure/intervention/issue of interest to achieve the outcome:__  {df$picot_t}  
    
    __Data analysis plan:__  {df$dataplan}  
    
    ## Variables requested
    __Forms list:__ {df$forms %>% unlist() %>% str_flatten(collapse = ', ')}  
    __Which wave?__ {df$waves %>% unlist() %>% str_flatten(collapse = ', ')}  
    __Another processed variable?__ {df$procvar}
    
    ## Other
    __Data in which extension?__ {df$ext}  
    __Observations__ {df$obs}
    
    ## Need approval from:  
    {df$comitee}  
    
    ## To approve or decline the proposal:
    1. Please, click [__HERE__](https://docs.google.com/spreadsheets/d/1MpIeNHxWx7wSNKvf_l6vVk2DO8BlXA6fKIob84-52fE/edit#gid=958972569);
    2. Log in to your Google account;
    3. Filter the data for the respective propid (__{df$propid}__) on column A;
    4. Change the status of the proposal on proper columns (columns R to V), possible choices:  
        - _0 Declined_  
        - _1 In cosideration_  
        - _2 Approved_  
        - _3 Not applicable_  
        - _4 Time out_ - There is a deadline of 1 week to define the status of the proposal.
    5. If there is no need for additional aproval on specific comitee, change the respective columns (column U and V) to '_3 Not applicable_'.
    ") %>% md,
  footer = glue("Brazilian High-Risk Cohort Study \n {now()}.") %>% md)
} 
email <- map(prop_req2, make_email)
email
```

### Config smtp mail
Run only the first time. Create a credentials file for sending email through Gmail. See https://rstudio.github.io/blastula/articles/sending_using_smtp.html.  
```{r eval=FALSE, include=FALSE}
create_smtp_creds_file(file = "gmail_creds",
                       user = "adm.projetoconexao@gmail.com",
                       provider = "gmail")
```

### Send emails
```{r}
# sending email function
sendemail <- function(email, cc=NULL, propid) {
  if(is.na(cc)) cc = NULL
    
  email %>%
   smtp_send(to = c("pedro.pan@unifesp.br", "gsalumjr@gmail.com", "andresimi@gmail.com", "rodrigoabressan@gmail.com"), 
             cc = cc  %>% str_split(", ") %>% flatten_chr(),
             from = "adm.projetoconexao@gmail.com",
             subject = str_c("BHRC Propposal ID: ", propid),
             credentials = creds_file("gmail_creds"))
  }

# Send emails to each subject.
email_messages <- pmap(lst(email = email,
                           cc = map(prop_req2, "comitee_mail"),
                           propid = map(prop_req2, "propid")),
                           safely(sendemail, quiet = F))
```

# Create database
## Filter
```{r}
reqd1 <- prop %>% 
  filter(approved1 >=2 & approved2 >=2 & approved3 >=2 & approved4 >=2 & approved5 >=2 & sent_data !=1) # filter requests

reqd2 <- reqd1 %>% 
  group_by(propid) %>% 
  group_split() %>% 
  set_names(str_c("p", reqd1$propid))
```

## Vars request
```{r}
removevars1 <- c("subjectid", "id", "dawbaid", "teacherid", 
                "apm_idinterview", "bld_geneticsid", "nreg7b", "nreg7b_1", "p_nreg7b", #W0W1
                "n2reg5a1_a", "n2reg5a2_a", "p_n2reg5a1_a", "p_n2reg5a2_a") # W2
removevars2 <- c("sp(a|b|c|d|e|m|n|o|p)1a", "sp(h|i|j|k|l|t)1b")

# remove these variables
f <- reqd2 %>% map("forms") %>% map(unlist)
reqfilt <- map(f, ~filter(dic_raw, form %in% .x) %>% # filter data in forms request
                   filter(!variable %in% removevars1) %>% # remove id variables1
                   filter(str_detect(variable, removevars2[1], negate = T)) %>% # remove id variables2
                   filter(str_detect(variable, removevars2[2], negate = T)))

# variables request
reqv <- reqfilt %>% map("variable")
```

## Waves request
```{r}
recwave <- "Screening=screening_arm_1; Wave0=wave0_arm_1; Wave1=wave1_arm_1; Wave2=wave2_arm_1; Wave2_Covid1=wave2_covid1_arm_1; Wave2_Covid2=wave2_covid2_arm_1"
reqw <- reqd2 %>% 
  map("waves") %>% 
  map(unlist) %>% 
  map(~rec(., rec = recwave))
```

## Data requests
```{r}
bhrc_raw <- import(bhrc_noidraw, setclass = "tibble")

reqd3 <- map2(reqv, reqw, ~select(bhrc_raw, ident, redcap_event_name, all_of(.x)) %>% 
                           filter(redcap_event_name %in% .y))
```

## Export data
```{r}
# settings
path <- "bhrc_data/Export/Export_data/"
reqprop <- reqd2 %>% map("proponent") %>% map(word,-1)
reqext <- reqd2 %>% map("ext")
reqpropid <- reqd2 %>% map("propid") %>% map(str_extract, "\\d*(?=_)")
reqfile <- pmap(lst(reqprop, reqpropid, reqext), ~str_c(path, ..1, "_", ..2, "BHRC_", str_replace_all(today(),"-","_"), ..3))
reqfile

# Export function
reqd_export <- function(x, file, ext) {
  if(ext==".rds") {
    export(x, file = file)
  }else if(ext==".sav") {
    haven::write_sav(x, file = file) # here
    file.zip <- str_c(file, ".zip")
    zip(zipfile = file.zip, files = file, mode = "cherry-pick")
    file.remove(file)
  }else{
    export(x, file = file)
    file.zip <- str_c(file, ".zip")
    zip(zipfile = file.zip, files = file, mode = "cherry-pick")
    file.remove(file)
  }
}
#reqd_export(reqd$p3, reqfile$p3, reqext$p3) # test

# Export
pmap(lst(reqd3, reqfile, reqext), ~reqd_export(x = ..1, file = ..2, ext = ..3))
```
