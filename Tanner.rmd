---
title: "Tanner"
author: "André Simioni"
date: "04 de março de 2020"
output: html_document
editor_options: 
  chunk_output_type: console
---
# Librarys and settings
First, read the README.md section about `settings.r` file.
```{r lib,  include=FALSE}
# Aditional librarys
library(broom)

# Load settings.r
setwd("~/Documentos/pCloud/Projetos/Analises/bhrc")
source("settings.r", echo = T)
```

# Get data from redcap
```{r}
bhrc_raw <- import(bhrc_noidraw, setclass = "tibble") %>%
  filter(redcap_event_name != "screening_arm_1" & redcap_event_name != "wave0_arm_1") %>%
  select(ident, redcap_event_name, gender, p_q23:p_q35)
bhrc_raw

# check
bhrc_raw$gender %>% as_label(prefix=T) %>% summary() # gender
bhrc_raw %>% wave(2) %>% sumfac
```

# Recode
```{r}
tanner <- bhrc_raw %>% 
  # gender condictionals
  mutate(across(p_q23:p_q30, ~if_else(gender == 2, ., NA_integer_)),
         across(p_q31:p_q35, ~if_else(gender == 1, ., NA_integer_))) %>% 
  # recode
  ## GIRLS: c("Does not know/Did not answer"=99,"Do not apply"=77,"Yes"=1,"No"=2,"Did not respond confidencial"=66)
  rec(p_q23, p_q25, p_q26, p_q27, rec = "99,66=NA; 77,2,3=0; else=copy", suffix = "") %>% # period, pregnant, abortion
  rec(p_q28, rec = "99,66=NA; 77=0; else=copy", suffix = "") %>% # how many childrens? c("Does not know/Did not answer"=99,"1 child"=1,"Do not apply"=77,"Did not respond confidencial"=66)
  rec(p_q29,p_q30, rec = "99,66=NA; 77=1; else=copy", suffix="") %>% # # Tanner: c("Does not know/Did not answer"=99,"Do not apply"=77,"Table 1"=1,"Table 2"=2,"Table 3"=3,"Table 4"=4,"Table 5"=5,"Did not respond confidencial"=66)
  ## BOYS: p_q31: ("Does not know/Did not answer"=99,"Do not apply"=77,"Yes"=1,"No"=2,"Did not respond confidencial"=66) | p_q32: c("Do not apply (I never got pregnant)"=3,"Does not know/Did not answer"=88,"Yes"=1,"No"=2,"Did not respond confidencial"=66,"Do not apply"=77)
  rec(p_q31, p_q32, rec = "99,66,88=NA; 77,2,3=0; else=copy", suffix = "") %>% 
  rec(p_q33, rec = "99,77=0; 88,66=NA; else=copy", suffix = "") %>%  # how many children: c("Do not apply (do not have chidren)"=99,"Do not apply"=77,"Does not know/Did not answer"=88,"Did not respond confidencial"=66,"I have one son"=1)
  rec(p_q34, p_q35, rec = "99,66=NA; 77=1; else=copy", suffix = "") %>%  # Tanner: c("Does not know/Did not answer"=99,"Do not apply"=77,"Table 1"=1,"Table 2"=2,"Table 3"=3,"Table 4"=4,"Table 5"=5,"Did not respond confidencial"=66)
  # set var label
  copy_labels(bhrc_raw) %>% 
  set_labels(p_q31, p_q32, labels = c(No=0, Yes=1)) %>% 
  set_labels(p_q23, p_q25, p_q26, p_q27, labels = c(No=0, Yes=1)) %>%  # period, pregnant, abortion
  set_labels(p_q24_ns, labels = c("Do not apply (I never menstruated)"=99,"Does not know/Did not answer"=88,"Do not apply"=77,"Did not respond confidencial"=66)) %>% 
  select(-gender)


# Summarys
tanner %>% sumfac(label=T)
tanner %>% get_label()
tanner$p_q25 %>% sjlabelled::as_label(prefix=T) %>% summary()
```

# Export
```{r}
tanner %>% export("bhrc_data/Proc/Tanner.rds")
```
