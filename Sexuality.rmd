---
title: "Sexuality"
author: "André Simioni"
date: "14/09/2020"
output: html_document
---

# Librarys and settings
First, read the README.md section about `settings.r` file.
```{r lib,  include=FALSE}
# Aditional librarys

# Load settings.r
setwd("/home/andresimi/Documentos/pCloud/Projetos/Analises/bhrc")
source("settings.r", echo = T)
```

# Get raw data
```{r}
bhrc <- import(bhrc_noidraw, setclass = "tibble") %>% 
  filter(redcap_event_name != "screening_arm_1" & redcap_event_name != "wave0_arm_1")
```

# Compute
```{r}
sex <- bhrc %>% 
  select(ident, redcap_event_name, p_q15:p_q16_ns, p_n2_q42:p_n2_q43, p_n2_q48:p_n2_q52) %>% 
  set_na(-1:-2, na = c(99, 999, 9999, 999999, 999999999))

# take a look
sex %>% get_label() %>% as.list()
sex %>% sumfac(-1:-2, .label = F)
```

# Export
```{r}
sex %>% export("bhrc_data/Proc/Sex.rds")
```

