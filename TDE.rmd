---
title: "HRC - Teste de Desempenho Escolar (TDE)"
author: "André Rafael Simioni"
date: "27 de julho de 2017"
output: html_document
editor_options: 
  chunk_output_type: console
---
# Librarys and settings
First, read the README.md section about `settings.r` file.
```{r lib,  include=FALSE}
# Aditional librarys
library(lubridate); library(lavaan); library(semPlot); library(ltm)

# Load settings.r
setwd("~/Documentos/pCloud/Projetos/Analises/bhrc")
source("settings.r", echo = T)
```

# Get raw data
W0 - TDE
Missings:  
- 272 weren't interviewed in this phase
- Writing: 48 99s
- Reading: 118 99s

Converting w1 to w0
*speech*
f_tde4a55 = p_tde4a54
f_tde4a56 = p_tde4a55
f_tde4a57 = p_tde4a56
f_tde4a58 = p_tde4a57
f_tde4a59 = p_tde4a58
f_tde4a61 = p_tde4a59
f_tde4a63 = p_tde4a60
f_tde4a64 = p_tde4a61
*writting*
f_tde1k = p_tde1j
f_tde1q = p_tde1k
f_tde1gg = p_tde1l

```{r}
# Get data
tde_raw <- import(bhrc_noidraw, setclass = "tibble") %>% 
  filter(!str_detect(redcap_event_name,"screening_arm_1"), !str_detect(redcap_event_name,"covid")) %>% 
  select(ident, redcap_event_name, starts_with("f_tde"), starts_with("p_tde"), starts_with("p_ta"))

# W0
tde_w0 <- tde_raw %>% 
  filter(redcap_event_name == "wave0_arm_1") %>% 
  select(ident, redcap_event_name, starts_with("f_tde")) %>% 
  rename_with(~str_remove(., "f_"), .cols = starts_with("f_")) %>% 
  mutate(tde_time = parse_date_time(tde_time, "H:M:S") %>% as.character("%M:%S"))
tde_w0 %>% names()
tde_w0$tde_time %>% summary.factor()

# W1
tde_w1 <- tde_raw %>% 
  filter(redcap_event_name == "wave1_arm_1") %>% 
  select(ident, redcap_event_name, starts_with("p_tde")) %>% 
  remove_empty_cols() %>% 
  rename(tde4a55 = p_tde4a54,
         tde4a56 = p_tde4a55,
         tde4a57 = p_tde4a56,
         tde4a58 = p_tde4a57,
         tde4a59 = p_tde4a58,
         tde4a61 = p_tde4a59,
         tde4a63 = p_tde4a60,
         tde4a64 = p_tde4a61,
         # writting
         tde1k = p_tde1j,
         tde1q = p_tde1k,
         tde1gg = p_tde1l) %>% 
  rename_with(~str_remove(., "p_"), .cols = starts_with("p_"))
tde_w1 %>% names()
tde_w1$tde_time %>% summary.factor()

# W2
tde_w2 <- tde_raw %>% 
  filter(redcap_event_name == "wave2_arm_1") %>% 
  select(ident, redcap_event_name, starts_with("p_tde")) %>% 
  rename_with(~str_remove(., "p_"), .cols = starts_with("p_")) %>% 
  remove_empty_cols()
tde_w2 %>% names()
tde_w2$tde_time %>% summary.factor()

# Bind rows
tdeb <- bind_rows(tde_w0, tde_w1, tde_w2) %>% 
  arrange(ident, redcap_event_name) %>% 
  set_na(tde_name, tde1a:tde1hh, tde4a1:tde4a70, na = 99) %>% 
  # recode tde write score after 10 consecutive errors
  mutate(tde1k = if_else(tde1a==0 & tde1b==0 & tde1c==0 & tde1d==0 & tde1e==0 & tde1f==0 & tde1g==0 & tde1h==0 & tde1i==0 & tde1j==0, 0L, tde1k), 
         tde1l = if_else(tde1b==0 & tde1c==0 & tde1d==0 & tde1e==0 & tde1f==0 & tde1g==0 & tde1h==0 & tde1i==0 & tde1j==0 & tde1k==0, 0L, tde1l), 
         tde1m = if_else(tde1c==0 & tde1d==0 & tde1e==0 & tde1f==0 & tde1g==0 & tde1h==0 & tde1i==0 & tde1j==0 & tde1k==0 & tde1l==0, 0L, tde1m), 
         tde1n = if_else(tde1d==0 & tde1e==0 & tde1f==0 & tde1g==0 & tde1h==0 & tde1i==0 & tde1j==0 & tde1k==0 & tde1l==0 & tde1m==0, 0L, tde1n), 
         tde1o = if_else(tde1e==0 & tde1f==0 & tde1g==0 & tde1h==0 & tde1i==0 & tde1j==0 & tde1k==0 & tde1l==0 & tde1m==0 & tde1n==0, 0L, tde1o), 
         tde1p = if_else(tde1f==0 & tde1g==0 & tde1h==0 & tde1i==0 & tde1j==0 & tde1k==0 & tde1l==0 & tde1m==0 & tde1n==0 & tde1o==0, 0L, tde1p), 
         tde1q = if_else(tde1g==0 & tde1h==0 & tde1i==0 & tde1j==0 & tde1k==0 & tde1l==0 & tde1m==0 & tde1n==0 & tde1o==0 & tde1p==0, 0L, tde1q), 
         tde1r = if_else(tde1h==0 & tde1i==0 & tde1j==0 & tde1k==0 & tde1l==0 & tde1m==0 & tde1n==0 & tde1o==0 & tde1p==0 & tde1q==0, 0L, tde1r), 
         tde1s = if_else(tde1i==0 & tde1j==0 & tde1k==0 & tde1l==0 & tde1m==0 & tde1n==0 & tde1o==0 & tde1p==0 & tde1q==0 & tde1r==0, 0L, tde1s), 
         tde1t = if_else(tde1j==0 & tde1k==0 & tde1l==0 & tde1m==0 & tde1n==0 & tde1o==0 & tde1p==0 & tde1q==0 & tde1r==0 & tde1s==0, 0L, tde1t), 
         tde1u = if_else(tde1k==0 & tde1l==0 & tde1m==0 & tde1n==0 & tde1o==0 & tde1p==0 & tde1q==0 & tde1r==0 & tde1s==0 & tde1t==0, 0L, tde1u), 
         tde1v = if_else(tde1l==0 & tde1m==0 & tde1n==0 & tde1o==0 & tde1p==0 & tde1q==0 & tde1r==0 & tde1s==0 & tde1t==0 & tde1u==0, 0L, tde1v), 
         tde1w = if_else(tde1m==0 & tde1n==0 & tde1o==0 & tde1p==0 & tde1q==0 & tde1r==0 & tde1s==0 & tde1t==0 & tde1u==0 & tde1v==0, 0L, tde1w), 
         tde1x = if_else(tde1n==0 & tde1o==0 & tde1p==0 & tde1q==0 & tde1r==0 & tde1s==0 & tde1t==0 & tde1u==0 & tde1v==0 & tde1w==0, 0L, tde1x), 
         tde1y = if_else(tde1o==0 & tde1p==0 & tde1q==0 & tde1r==0 & tde1s==0 & tde1t==0 & tde1u==0 & tde1v==0 & tde1w==0 & tde1x==0, 0L, tde1y), 
         tde1z = if_else(tde1p==0 & tde1q==0 & tde1r==0 & tde1s==0 & tde1t==0 & tde1u==0 & tde1v==0 & tde1w==0 & tde1x==0 & tde1y==0, 0L, tde1z), 
         tde1aa = if_else(tde1q==0 & tde1r==0 & tde1s==0 & tde1t==0 & tde1u==0 & tde1v==0 & tde1w==0 & tde1x==0 & tde1y==0 & tde1z==0, 0L, tde1aa), 
         tde1bb = if_else(tde1r==0 & tde1s==0 & tde1t==0 & tde1u==0 & tde1v==0 & tde1w==0 & tde1x==0 & tde1y==0 & tde1z==0 & tde1aa==0, 0L, tde1bb), 
         tde1cc = if_else(tde1s==0 & tde1t==0 & tde1u==0 & tde1v==0 & tde1w==0 & tde1x==0 & tde1y==0 & tde1z==0 & tde1aa==0 & tde1bb==0, 0L, tde1cc), 
         tde1dd = if_else(tde1t==0 & tde1u==0 & tde1v==0 & tde1w==0 & tde1x==0 & tde1y==0 & tde1z==0 & tde1aa==0 & tde1bb==0 & tde1cc==0, 0L, tde1dd), 
         tde1ee = if_else(tde1u==0 & tde1v==0 & tde1w==0 & tde1x==0 & tde1y==0 & tde1z==0 & tde1aa==0 & tde1bb==0 & tde1cc==0 & tde1dd==0, 0L, tde1ee), 
         tde1ff = if_else(tde1v==0 & tde1w==0 & tde1x==0 & tde1y==0 & tde1z==0 & tde1aa==0 & tde1bb==0 & tde1cc==0 & tde1dd==0 & tde1ee==0, 0L, tde1ff), 
         tde1gg = if_else(tde1w==0 & tde1x==0 & tde1y==0 & tde1z==0 & tde1aa==0 & tde1bb==0 & tde1cc==0 & tde1dd==0 & tde1ee==0 & tde1ff==0, 0L, tde1gg), 
         tde1hh = if_else(tde1x==0 & tde1y==0 & tde1z==0 & tde1aa==0 & tde1bb==0 & tde1cc==0 & tde1dd==0 & tde1ee==0 & tde1ff==0 & tde1gg==0, 0L, tde1hh), 
  # time variable in seconds
         tde_time = parse_date_time(tde_time, "M:S") %>% as.numeric(), 
  # Count missings
         #na_wr2 = is.na(across(tde1a:tde1hh)) %>% rowSums(na.rm = T),
         na_wr = select(., tde1a:tde1hh) %>% is.na() %>% rowSums(),
         na_read = select(., tde4a1:tde4a70) %>% is.na() %>% rowSums(), 
         na_ar = select(., tde3a1:tde3g5) %>% is.na() %>% rowSums())

tdeb %>% summary()
```

# Not literate var
## Create
Create dataframe to check the protocols searching for notes indicating not literacy and coding this subjects with 1. Then we recoded the missing values of these subjects to 0 (wrong). Don't need to run anymore.
### W0
```{r eval=FALSE,  include=FALSE}
# Get data from other dataframes
reg <- import("bhrc_data/Proc/Reg.rds", setclass="tibble") %>% select(ident, redcap_event_name, subjectid, instlevel)
speechtprepos <- import("Data/Proc/SpeechtPrePos.rds", setclass = "tibble") %>% select(ident, redcap_event_name, f_ocor1)

# Create check dataframe - remove the complete rows and all missing rows
check_w0 %>% nrow()
check_w0 %>% names()
check_w0 <- tdeb %>% 
  filter(redcap_event_name == "wave0_arm_1") %>% 
  #filter(na_wr!=0) %>% 
  #filter_at(-1:-2, any_vars(complete.cases(.)))
  #filter(na_read!=0) %>% 
  #filter_at(vars(tde1a:tde1hh, tde4a1:tde4a70), any_vars(is.na(.)))
  filter(na_wr!=0 | na_read!=0)# %>% 
  #filter(na_wr!=34 | na_read!=70) %>% 
  left_join(idsocio) %>%
  left_join(speechtprepos) %>% 
  transmute(ident, subjectid, redcap_event_name, 
            f_ocor1 = str_to_lower(f_ocor1), 
            notliterate = if_else(str_detect(f_ocor1, "(alfa|ler|escrev|autista|down|hidrocef)"), 1L, NA_integer_), 
            lit_notes = NA, 
            na_wr, na_read, na_ar)
check_w0
check_w0$notliterate %>% summary.factor()

# Export
#check_w0 %>% export("Data/notliterate_w0.xlsx")
```
### W1
```{r eval=FALSE,  include=FALSE}
check_w1 <- tde_w1 %>%
  filter(na_all==0) %>% # remove not interviewed
  filter(na_wr!=0 | na_read!=0 | na_readc!=0) %>% # remove the ones with complete data
  left_join(idsocio) %>% # get subjectid var
  select(ident, subjectid, redcap_event_name, na_wr, na_read, na_readc, everything(), -instlevel, -(s2_complete:psico_hour2_ns), -matches("time"), -ends_with("fl")) %>% 
  add_column(lit_notes=NA, .after = "redcap_event_name")
check_w1
```


## Get 
```{r}
# Import spreadsheet already checked an extract ids
notliterate <- import_list("bhrc_data/notliterate.xlsx", setclass = "tibble") %>% 
  bind_rows() %>% 
  arrange(ident, redcap_event_name) %>%
  filter(notliterate==1) %>% 
  select(ident, redcap_event_name, notliterate) %>% 
  # set var/val labels
  set_labels(notliterate, labels = c(no=0, yes=1, "by phone interview"=2)) %>% 
  var_labels(notliterate="Not literate according evaluator notes")
notliterate

# Recode missing of notliterate to 0
tde_w0 <- tde_w0 %>% mutate_at(vars(tde1a:tde1hh, tde4a1:tde4a70), ~if_else(notliterate_w0$notliterate==1 & is.na(.), 0L, .))

```

## Latent
### Write
After removing subjects with missing in all write variables: 2196 (2191)
```{r}
# Filter out subjects with missing in all variables
write_w0 <- tde_w0 %>% select(ident, redcap_event_name, tde1a:tde1hh) %>% 
  filter_at(-1:-2, any_vars(complete.cases(.))) %>% 
  mutate_at(vars(-ident, -redcap_event_name), ordered)
#write_w0 %>% skim()
write_w0 %>% VIM::aggr(number=T, prop=F, cex.axis=.7) # missing patterns

# Unidimentional Writing score
model<- "# Latent Variables Definition
tde_write_fl =~ tde1a + tde1b + tde1c + tde1d + tde1e + tde1f + tde1g + tde1h + tde1i + tde1j + tde1k + tde1l + tde1m + tde1n + tde1o + 
                tde1p + tde1q + tde1r + tde1s + tde1t + tde1u + tde1v + tde1w + tde1x + tde1y + tde1z + tde1aa + tde1bb + tde1cc + tde1dd + 
                tde1ee + tde1ff + tde1gg + tde1hh"
fit_write_w0 <- cfa(model, data=write_w0, estimator="WLSMV", std.lv=T, missing="pairwise")
fit_write_w0 %>% summary(fit.measures=T, standardized=T)
#fit_write_w0 %>% semPaths(what="paths", intercepts=F, residuals=F, sizeLat = 6, sizeMan = 4, sizeMan2=4) # Diagram
#fit_write_w0 %>% reliability() # Omega
facload <- fit_write_w0 %>% predict() # Extract factor loadings

# Store factor loading in a identified dataframe
write_fl_w0 <- write_w0 %>% select(ident, redcap_event_name) %>% cbind(facload) %>% as.data.frame()
#write_fl_w0 %>% skim()
rm(model, facload)
```

### Read
After removing subjects with missing in all read variables: 2156 (2121)
```{r}
# Filter out subjects with missing in all variables
read_w0 <- tde_w0 %>% select(ident, redcap_event_name, tde4a1:tde4a70) %>%
  filter_at(-1:-2, any_vars(complete.cases(.))) %>% 
  mutate_at(vars(-ident, -redcap_event_name), ordered)
#read_w0 %>% skim()
read_w0 %>% VIM::aggr(number=T, prop=F, cex.axis=.7) # missing patterns

# Unidimentional Reading score
model<- "# Latent Variable definition
tde_read_fl =~ tde4a1 + tde4a2 + tde4a3 + tde4a4 + tde4a5 + tde4a6 + tde4a7 + tde4a8 + tde4a9 + tde4a10 + tde4a11 + tde4a12 + tde4a13 + 
               tde4a14 + tde4a15 + tde4a16 + tde4a17 + tde4a18 + tde4a19 + tde4a20 + tde4a21 + tde4a22 + tde4a23 + tde4a24 + tde4a25 + 
               tde4a26 + tde4a27 + tde4a28 + tde4a29 + tde4a30 + tde4a31 + tde4a32 + tde4a33 + tde4a34 + tde4a35 + tde4a36 + tde4a37 + 
               tde4a38 + tde4a39 + tde4a40 + tde4a41 + tde4a42 + tde4a43 + tde4a44 + tde4a45 + tde4a46 + tde4a47 + tde4a48 + tde4a49 + 
               tde4a50 + tde4a51 + tde4a52 + tde4a53 + tde4a54 + tde4a55 + tde4a56 + tde4a57 + tde4a58 + tde4a59 + tde4a60 + tde4a61 + 
               tde4a62 + tde4a63 + tde4a64 + tde4a65 + tde4a66 + tde4a67 + tde4a68 + tde4a69 + tde4a70"
fit_read_w0 <- cfa(model, data=read_w0, estimator="WLSMV", std.lv=T, missing="pairwise")
fit_read_w0 %>% summary(fit.measures=T, standardized=T)
#fit_read_w0 %>% semPaths(what="paths", intercepts=F, residuals=F, sizeLat = 6, sizeMan = 4, sizeMan2=4) # Diagram
#fit_read_w0 %>% reliability() # Omega
facload <- fit_read_w0 %>% predict() # Extract factor loadings

# Store factor loading in a identified dataframe
read_fl_w0 <- read_w0 %>% select(ident, redcap_event_name) %>% cbind(facload) %>% as.data.frame()
#read_fl_w0 %>% skim()
rm(model, facload)
```
## Merge W0
```{r}
tdefl_w0 <- tde_w0 %>% left_join(write_fl_w0) %>% left_join(read_fl_w0)
tdefl_w0 %>% names()
#tdefl_w0 %>% skim()
```

# W1
Missings:  
- 631 not interviewed  

## TDE W1
### Setting 2
```{r}
# Recode other missing as error (0)
tde_w1 <- tde_w1 %>% 
  mutate_at(vars(tde_name:tde1l, tde4a1:tde4a61), ~if_else(notliterate_w1$notliterate==1 & is.na(.), 0L, .))
  #mutate_at(vars(tde_name:tde1l), ~if_else(tde_w1$na_wr==F, recode(., "NA=0L"), .)) %>% 
  #mutate_at(vars(tde4a1:tde4a61), ~if_else(tde_w1$na_read!=61, recode(., "NA=0L"), .))

# Check
#select(tde_w1, tde_name:tde1l) %>% ncol()
#select(tde_w1, tde4a1:tde4a61) %>% ncol()
#select(tde_w1, p_ta1:p_ta10) %>% ncol()

# See the patterns
#tde_w1 %>% group_by_at(vars(tde1a:tde1l)) %>% count() %>% arrange(desc(n)) %>% View()
#tde_w1 %>% group_by_at(vars(tde4a1:tde4a61)) %>% count() %>% arrange(desc(n)) %>% View()
#tde_w1 %>% filter(ident==450) %>% View()
```


### Latent
#### Write
After removing subjects with missing in all write variables: 1773
```{r}
# Filter out subjects with missing in all variables
write_w1 <- tde_w1 %>% select(ident, redcap_event_name, tde_name:tde1l) %>% 
  filter_at(-1:-2, any_vars(complete.cases(.))) %>% 
  mutate_at(vars(-ident, -redcap_event_name), ordered)
#write_w1 %>% skim()
write_w1 %>% VIM::aggr(number=T, prop=F, cex.axis=0.5)

## Unidimentional Writing score
model<- "# Latent Variables Definition
tde_write_fl =~ tde_name + tde1a + tde1b + tde1c + tde1d + tde1e + tde1f + tde1g + tde1h + tde1i + tde1j + tde1k + tde1l"
fit_write_w1 <- cfa(model, data=write_w1, estimator="WLSMV", std.lv=T, missing="pairwise")
fit_write_w1 %>% summary(fit.measures=T, standardized=T)
#fit_read_w1 %>% semPaths(what="paths", intercepts=F, residuals=F, sizeLat = 6, sizeMan = 4, sizeMan2=4) # Diagram
#fit_read_w1 %>% reliability() # Omega
facload <- fit_write_w1 %>% predict() # Extract factor loadings

# Store factor loading in a identified dataframe
write_fl_w1 <- write_w1 %>% select(ident, redcap_event_name) %>% cbind(facload) %>% as.data.frame()
#write_fl_w1 %>% skim()
rm(model, facload)
```

#### Read
After removing subjects with missing in all write variables: 1775
```{r}
# Filter out subjects with missing in all variables
read_w1 <- tde_w1 %>% select(ident, redcap_event_name, tde4a1:tde4a61) %>% 
  filter_at(-1:-2, any_vars(complete.cases(.))) %>% 
  mutate_at(vars(-ident, -redcap_event_name), ordered)
#read_w1 %>% skim()
read_w1 %>% VIM::aggr(number=T, prop=F, cex.axis=0.5)

# Unidimentional Reading score
model<- "# Latent Variable definition
tde_read_fl =~ tde4a1 + tde4a2 + tde4a3 + tde4a4 + tde4a5 + tde4a6 + tde4a7 +  tde4a9 + tde4a10 + tde4a11 + tde4a12 + tde4a13 + 
               tde4a14 + tde4a15 + tde4a16 + tde4a18 + tde4a19 + tde4a20 + tde4a21 + tde4a22 + tde4a23 + tde4a24 + tde4a25 + 
               tde4a26 + tde4a27 + tde4a28 + tde4a29 + tde4a30 + tde4a31 + tde4a32 + tde4a33 + tde4a34 + tde4a35 + tde4a36 + tde4a37 + 
               tde4a38 + tde4a39 + tde4a40 + tde4a41 + tde4a42 + tde4a43 + tde4a44 + tde4a45 + tde4a46 + tde4a47 + tde4a48 + tde4a49 + 
               tde4a50 + tde4a51 + tde4a52 + tde4a53 + tde4a54 + tde4a55 + tde4a56 + tde4a57 + tde4a58 + tde4a59 + tde4a60 + tde4a61
#tde4a8  ~~ tde4a17
"
fit_read_w1 <- cfa(model, data=read_w1, estimator="WLSMV", std.lv=T, missing="pairwise")
fit_read_w1 %>% summary(fit.measures=T, standardized=T)
fit_read_w1 %>% modindices(sort. = T) %>% as.tibble() %>% filter(lhs=="tde4a8"|lhs=="tde4a17")
#fit_read_w1 %>% lavInspect("zero.cell.tables")
#fit_read_w1 %>% semPaths(what="paths", intercepts=F, residuals=F, sizeLat = 6, sizeMan = 4, sizeMan2=4) # Diagram
#fit_read_w1 %>% reliability() # Omega
facload <- fit_read_w1 %>% predict() # Extract factor loadings

# Store factor loading in a identified dataframe
read_fl_w1 <- read_w1 %>% select(ident, redcap_event_name) %>% cbind(facload) %>% as.tibble()
#read_fl_w1 %>% skim()
rm(model, facload)
```
## Merge W1
```{r}
tdefl_w1 <- tde_w1 %>% left_join(write_fl_w1) %>% left_join(read_fl_w1)
tdefl_w1 %>% names()
#tdefl_w1 %>% skim()
```

# Merge Waves
```{r}
tdefl <- tdefl_w0 %>% 
  bind_rows(tdefl_w1) %>% 
  arrange(ident, redcap_event_name) %>% 
  select(-na_all, -na_wr, -na_readc, -na_read) %>% 
  var_labels(#tde_arithm_wr_w0="TDE: Arithmetics Writen evaluation", 
             tde_write_fl="TDE: Factor Loading of Unidim Model of Writing skills", 
             tde_read_fl="TDE: Factor Loading of Unidim Model of Reading skills", 
             tde_time="TDE: Reading time in seconds", 
             readcomp_fl="Reading compreension fac load")
#tdefl %>% names()
#tdefl %>% filter(redcap_event_name=="wave1_arm_1") %>% select(tde_read_fl, tde_write_fl)
tdefl %>% select(ident, redcap_event_name, tde_time)
```

# Reading comp.
## Setting
```{r}
readc_o <- tde_raw %>% 
  select(ident, redcap_event_name, starts_with("p_ta")) %>% 
  filter_at(-1:-2, any_vars(complete.cases(.))) %>% 
  # Recode Value labels
  rec(p_ta1:p_ta10, rec = "2,99=0; else=copy", suffix = "")# %>% 
  #mutate(across(starts_with("p_ta"), ordered))
readc_o

# CFA
model <-'readcomp_fl =~ p_ta1 + p_ta2 + p_ta3 + p_ta4 + p_ta5 + p_ta6 + p_ta7 + p_ta8 + p_ta9 + p_ta10'

#Model
readc_fit <- cfa(model, data=readc_o, std.lv=T, estimator="WLSMV", missing="pairwise")
readc_mo %>% summary(fit.measures=T)
facload <- readc_mo %>% predict() # Extract factor loadings
readc_fl <- readc_lv %>% select(ident, redcap_event_name) %>% cbind(facload) %>% as.data.frame()

# IRT
readci <- select(readc_o, -ident, -redcap_event_name)
fit_read <- readci %>% grm(Hessian=T); fit_read
plot(fit_read,  type = "IIC",  items = 0)
scores <- factor.scores.grm(fit_read, resp.patterns = readci)
scores_data <- scores$score.dat %>% as_tibble %>% left_join(readc_o, .) %>% view()
#hist(scores$score.dat$z1)
rm(model, facload)
```
## Merge
```{r}
readc <- readc_o %>% 
  left_join(readc_fl) %>%
  var_labels(readcomp_fl="Reading compreension fac loading")
readc
```

# Export
```{r export,  eval=FALSE,  include=FALSE}
tdefl %>% export("Data/Proc/TDE.rds")
```

# Aritm
```{r}
# Recode missing of notliterate to 0
tde_w0 <- tde_w0 %>% mutate_at(vars(tde1a:tde1hh, tde4a1:tde4a70), ~if_else(notliterate_w0$notliterate==1 & is.na(.), 0L, .))

# How many vars in each dimension
select(tdeb, tde1a:tde1hh) %>% ncol()
select(tdeb, tde4a1:tde4a70) %>% ncol()
select(tdeb, tde3a1:tde3g5) %>% ncol()

## Recode other missings as error (0)
tde_w0 <- tde_w0 %>% 
  #mutate_at(vars(tde1a:tde1hh), ~if_else(tde_w0$na_wr<34, recode(., "NA=0L"), .)) %>% 
  #mutate_at(vars(tde4a1:tde4a70), ~if_else(tde_w0$na_wr<70, recode(., "NA=0L"), .)) %>%
  # Compute tde arithmetics
  mutate(tde_arithm_wr=case_when(between(notliterate_w0$instlevel, 7, 12) & tde3g1==1 & tde3g2==1 & tde3g3==1 & tde3g4==1 & tde3g5==1 ~ 1, 
                                 between(notliterate_w0$instlevel, 7, 12) & (tde3g1!=1 | tde3g2!=1 | tde3g3!=1 | tde3g4!=1 | tde3g5!=1) ~ 0, 
                                 notliterate_w0$instlevel==6 & tde3f1==1 & tde3f2==1 & tde3f3==1 & tde3f4==1 & tde3f5==1 ~ 1, 
                                 notliterate_w0$instlevel==6 & (tde3f1!=1 | tde3f2!=1 | tde3f3!=1 | tde3f4!=1 | tde3f5!=1) ~ 0, 
                                 notliterate_w0$instlevel==5 & tde3e1==1 & tde3e2==1 & tde3e3==1 & tde3e4==1 & tde3e5==1 ~ 1, 
                                 notliterate_w0$instlevel==5 & (tde3e1!=1 | tde3e2!=1 | tde3e3!=1 | tde3e4!=1 | tde3e5!=1) ~ 0, 
                                 notliterate_w0$instlevel==4 & tde3d1==1 & tde3d2==1 & tde3d3==1 & tde3d4==1 & tde3d5==1 ~ 1, 
                                 notliterate_w0$instlevel==4 & (tde3d1!=1 | tde3d2!=1 | tde3d3!=1 | tde3d4!=1 | tde3d5!=1) ~ 0, 
                                 notliterate_w0$instlevel==3 & tde3c1==1 & tde3c2==1 & tde3c3==1 & tde3c4==1 & tde3c5==1 & tde3c6==1 ~ 1, 
                                 notliterate_w0$instlevel==3 & (tde3c1!=1 | tde3c2!=1 | tde3c3!=1 | tde3c4!=1 | tde3c5!=1 | tde3c6!=1) ~ 0, 
                                 notliterate_w0$instlevel==2 & tde3b1==1 & tde3b2==1 & tde3b3==1 & tde3b4==1 & tde3b5==1 ~ 1, 
                                 notliterate_w0$instlevel==2 & (tde3b1!=1 | tde3b2!=1 | tde3b3!=1 | tde3b4!=1 | tde3b5!=1) ~ 0, 
                                 notliterate_w0$instlevel==1 & tde3a1==1 & tde3a2==1 & tde3a3==1 & tde3a4==1 ~ 1, 
                                 notliterate_w0$instlevel==1 & (tde3a1!=1 | tde3a2!=1 | tde3a3!=1 | tde3a4!=1) ~ 0)) %>% 
  var_labels(tde_arithm_wr="TDE: achieved math habilities for his instruction level") %>% 
  set_labels(tde_arithm_wr, labels = c(Achieved=1, 'Not achieved'=0))

tde_w0$tde_arithm_wr %>% as_label() %>% summary()

tde_w0 %>% filter(tde_arithm_wr==1)


# Check
#tde_w0 %>% group_by_at(vars(tde1a:tde1hh)) %>% count() %>% arrange(desc(n)) %>% View()
#tde_w0 %>% group_by_at(vars(tde4a1:tde4a70)) %>% count() %>% arrange(desc(n)) %>% View()
#tde_w0 %>% group_by_at(vars(tde3a1:tde3g5)) %>% count() %>% arrange(desc(n)) %>% View()
#tde_w0 %>% mutate_all(factor) %>% summary()

#tde_w0 %>% filter(ident==777) %>% View()

# Summaries
idsocio$instlevel %>% get_labels()
idsocio$instlevel %>% sumfac()
tde_w0$tde_arithm_wr %>% sumfac()
```
