---
title: "Treatment"
author: "André Simioni"
date: "03/08/2020"
output: html_document
editor_options: 
  chunk_output_type: console
---

# Librarys and settings
First, read the README.md section about `settings.r` file.
```{r lib,  include=FALSE}
# Aditional librarys


# Load settings.r
source("settings.r", echo = T)
```

# Get raw data
```{r}
treat_raw <- import(bhrc_noidraw, setclass = "tibble") %>% 
  filter(redcap_event_name != "screening_arm_1") %>% 
  select(ident, redcap_event_name, 
         fat30:fat33d, # dom w0
         nfat30:nfat33d # dom w1w2
         )
```

# Compute domiciliar treatment
```{r}
# Defining drugs classes (see neuro-p protocol)
treat_raw$n2fat31a1 %>% get_labels()
srsi <- 1:6
tca <- 7:10
snri <- 12:14
bup <- 15
anf <- 16
bzd <- 17:21
ehum <- c(22:24, 27, 40)
atpsi <- 29:39
others <- c(11, 25, 41)

# Function "is in this drug class?"
isdrugclass <- function(df, drugclass, period) {
  if (period == "lifetime") {
    if_else((pull(df, fat31b1) %in% drugclass & pull(df, fat31c1) == 1) |
            (pull(df, fat31b2) %in% drugclass & pull(df, fat31c2) == 1) |
            (pull(df, fat31b3) %in% drugclass & pull(df, fat31c3) == 1) |
            (pull(df, fat31b4) %in% drugclass & pull(df, fat31c4) == 1) |
            (pull(df, fat31b5) %in% drugclass & pull(df, fat31c5) == 1) |
            (pull(df, fat31b6) %in% drugclass & pull(df, fat31c6) == 1), 1, 0)
  } else if (period == "current") {
    if_else((pull(df, fat31b1) %in% drugclass & pull(df, fat31d1) == 1) |
            (pull(df, fat31b2) %in% drugclass & pull(df, fat31d2) == 1) |
            (pull(df, fat31b3) %in% drugclass & pull(df, fat31d3) == 1) |
            (pull(df, fat31b4) %in% drugclass & pull(df, fat31d4) == 1) |
            (pull(df, fat31b5) %in% drugclass & pull(df, fat31d5) == 1) |
            (pull(df, fat31b6) %in% drugclass & pull(df, fat31d6) == 1), 1, 0)
  }
  
}

# Check if a wave has the variable
#var <- "fat31med$"
#treat_raw %>% select(matches(var)) %>% get_label %>% as.list()
#0:2 %>% map(~wave(treat_raw,.) %>% select(matches(var)) %>% sumfac(.label = F, .max = 20))

# Compute
treat <- treat_raw %>%
  # Unite medications variables of w0/w1/w2
  ## Medication use
  unite(fat31med, fat31med, nfat31med, sep="") %>% 
  unite(fat31a1, fat31a1, nfat31a1, sep="") %>% # fat 31a1, NA for W2
  unite(fat31b1, fat31b1, nfat31b1, n2fat31a1, sep="") %>%
  unite(fat31c1, fat31c1, nfat31c1, sep="") %>%
  unite(fat31d1, fat31d1, nfat31d1, sep="") %>%
  unite(fat31e1, fat31e1, nfat31e1, sep="") %>%
  unite(fat31f1, fat31f1, nfat31f1, sep="") %>%
  unite(fat31g1, fat31g1, nfat31g1, sep="") %>% 
  unite(fat31a2, fat31a2, nfat31a2, sep="") %>% # fat 31a2
  unite(fat31b2, fat31b2, nfat31b2, n2fat31a2, sep="") %>%
  unite(fat31c2, fat31c2, nfat31c2, sep="") %>%
  unite(fat31d2, fat31d2, nfat31d2, sep="") %>%
  unite(fat31e2, fat31e2, nfat31e2, sep="") %>%
  unite(fat31f2, fat31f2, nfat31f2, sep="") %>%
  unite(fat31g2, fat31g2, nfat31g2, sep="") %>% 
  unite(fat31a3, fat31a3, nfat31a3, sep="") %>% # fat 31a3
  unite(fat31b3, fat31b3, nfat31b3, n2fat31a3, sep="") %>%
  unite(fat31c3, fat31c3, nfat31c3, sep="") %>%
  unite(fat31d3, fat31d3, nfat31d3, sep="") %>%
  unite(fat31e3, fat31e3, nfat31e3, sep="") %>%
  unite(fat31f3, fat31f3, nfat31f3, sep="") %>%
  unite(fat31g3, fat31g3, nfat31g3, sep="") %>% 
  unite(fat31a4, fat31a4, nfat31a4, sep="") %>% # fat 31a4
  unite(fat31b4, fat31b4, nfat31b4, n2fat31a4, sep="") %>%
  unite(fat31c4, fat31c4, nfat31c4, sep="") %>%
  unite(fat31d4, fat31d4, nfat31d4, sep="") %>%
  unite(fat31e4, fat31e4, nfat31e4, sep="") %>%
  unite(fat31f4, fat31f4, nfat31f4, sep="") %>%
  unite(fat31g4, fat31g4, nfat31g4, sep="") %>%
  unite(fat31a5, fat31a5, sep="") %>% # fat 31a5 
  unite(fat31b5, fat31b5, n2fat31a5, sep="") %>%
  unite(fat31c5, fat31c5, nfat31c5, sep="") %>%
  unite(fat31d5, fat31d5, nfat31d5, sep="") %>%
  unite(fat31e5, fat31e5, nfat31e5, sep="") %>%
  unite(fat31f5, fat31f5, nfat31f5, sep="") %>%
  unite(fat31g5, fat31g5, nfat31g5, sep="") %>%
  unite(fat31b6, n2fat31a6, sep="") %>% # fat 31a6 just w2
  unite(fat31c6, nnfat31c6, sep="") %>%
  unite(fat31d6, nfat31d6, sep="") %>%
  unite(fat31e6, nfat31e6, sep="") %>%
  unite(fat31f6, nfat31f6, sep="") %>%
  unite(fat31g6, nfat31g6, sep="") %>% 
  mutate_at(vars(fat31med:fat31g5, fat31b6:fat31g6), ~str_remove_all(., "NA") %>% na_if("")) %>% 
  mutate_at(vars(matches("^fat31(b|c|d|e|f|g)")), as.numeric) %>% 
  ## Treatment 
  unite(fat30, fat30, nfat30, sep="") %>% 
  unite(fat31a, fat31a, nfat31a, sep="") %>% 
  unite(fat32, nfat32, sep = "") %>% 
  unite(fat32a, nfat32a, sep = "") %>% 
  unite(fat32b, nfat32b, sep = "") %>% 
  unite(fat32c, nfat32c, sep = "") %>% 
  unite(fat32d, nfat32d, sep = "") %>% 
  unite(fat32e, nfat32e, sep = "") %>% 
  unite(fat32f, nfat32f, sep = "") %>% 
  unite(fat33a, nfat33a, sep = "") %>% 
  unite(fat33b, nfat33b, sep = "") %>% 
  unite(fat33c, nfat33c, sep = "") %>% 
  unite(fat33d, nfat33d, sep = "") %>% 
  unite(fat31b1_1, fat31b1_1, nfat31b1_1, sep="") %>% 
  unite(fat31b1_1_1, fat31b1_1_1, nfat31b1_1_1, sep="") %>% 
  unite(fat31b2_1y, fat31b2_1y, nfat31b2_y, sep="") %>%
  unite(fat31b2_1m, fat31b2_1m, nfat31b2_m, sep="") %>% 
  unite(fat31b3_1, fat31b3_1, nfat31b3_1, sep="") %>% 
  unite(fat31b4_1, fat31b4_1, nfat31b4_1, sep="") %>% 
  unite(fat31b5_1, fat31b5_1, nfat31b5_1, sep="") %>% 
  unite(fat31b6_1, fat31b6_1, nfat31b6_1, sep="") %>% 
  unite(fat31c1_1, fat31c1_1, nfat31c1_1, sep="") %>% 
  unite(fat31c2_1, fat31c2_1, nfat31c2_1, sep="") %>% 
  unite(fat31c4_1, fat31c4_1, nfat31c4_1, sep="") %>% 
  mutate_at(vars(fat30, fat31a, fat32:fat33d, fat31b1_1:fat31b6_1, fat31c1_1, fat31c2_1, fat31c4_1), ~str_remove_all(., "NA") %>% na_if("") %>% as.numeric) %>% 
  # Recode
  set_na(starts_with("fat30"), starts_with("nfat30"), starts_with("fat31"), starts_with("nfat31"), starts_with("fat32"), starts_with("nfat32"), starts_with("fat33"), starts_with("nfat33"), na = 99) %>% 
  # SRSI
  mutate(ssri_lifetime = isdrugclass(., srsi, "lifetime"),
         ssri_current = isdrugclass(., srsi, "current"),
         # TCA
         tca_lifetime = isdrugclass(., tca, "lifetime"),
         tca_current = isdrugclass(., tca, "current"),
         # SNRI
         snri_lifetime = isdrugclass(., snri, "lifetime"),
         snri_current = isdrugclass(., snri, "current"),
         # BUP
         bup_lifetime = isdrugclass(., bup, "lifetime"),
         bup_current = isdrugclass(., bup, "current"),
         # ANF
         anf_lifetime = isdrugclass(., anf, "lifetime"),
         anf_current = isdrugclass(., anf, "current"),
         # BZD
         bzd_lifetime = isdrugclass(., bzd, "lifetime"),
         bzd_current = isdrugclass(., bzd, "current"),
         # mstab
         moodstab_lifetime = isdrugclass(., ehum, "lifetime"),
         moodstab_current = isdrugclass(., ehum, "current"),
         # ATPS
         atps_lifetime = isdrugclass(., atpsi, "lifetime"),
         atps_current = isdrugclass(., atpsi, "current"),
         # OTHERS
         oth_lifetime = isdrugclass(., others, "lifetime"),
         oth_current = isdrugclass(., others, "current"),
         # General
         med_lifetime = if_else(ssri_lifetime == 1 | tca_lifetime == 1 | snri_lifetime == 1 | bup_lifetime == 1 | anf_lifetime == 1 |
                                bzd_lifetime == 1 | moodstab_lifetime == 1 | atps_lifetime == 1 | oth_lifetime == 1, 1, 0),
         med_current = if_else(ssri_current == 1 | tca_current == 1 | snri_current == 1 | bup_current == 1 | anf_current == 1 |
                               bzd_current == 1 | moodstab_current == 1 | atps_current == 1 | oth_current == 1, 1, 0)) %>% 
  # set var/val labels
  set_labels(ends_with("current"), ends_with("lifetime"), labels = c(Yes=1, No=0)) %>% 
  copy_labels(treat_raw) %>% 
  var_labels(anf_current = "Current anfetamine use", 
             anf_lifetime = "Lifetime anfetamine use", 
             ssri_current = "Current serotonine reuptake inhibitor use", 
             ssri_lifetime = "Lifetime serotonine reuptake inhibitor use",
             snri_current = "Current serotonin and norepinephrine reuptake inhibitors use", 
             snri_lifetime = "Lifetime serotonine and norepinephrine reuptake inhibitor use", 
             bup_current = "Current bupropion use", 
             bup_lifetime = "Lifetime bupropion use", 
             tca_current = "Current triciclic use", 
             tca_lifetime = "Lifetime triciclic use", 
             bzd_current = "Current benzo use", 
             bzd_lifetime = "Lifetime benzo use", 
             moodstab_current = "Current mood stabilizer use", 
             moodstab_lifetime = "Lifetime mood stabilizer use", 
             atps_current = "Current antipsychotic use", 
             atps_lifetime = "Lifetime antipsychotic use", 
             oth_current = "Current other medication use",
             oth_lifetime = "Lifetime other medication use",
             med_current = "Current any medication use", 
             med_lifetime ="Lifetime any medication use")
treat %>% names()
treat %>% 
  #wave(0) %>% 
  sumfac(.label = T)
```

# Export
```{r}
treat %>% export("bhrc_data/Proc/Treatment.rds")
```

