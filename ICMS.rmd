---
title: "ICMS"
author: "André Simioni"
date: "15/10/2020"
output: html_document
---
Adapted version of the game addiction scale

# Librarys and settings
First, read the README.md section about `settings.r` file.
```{r lib,  include=FALSE}
# Aditional librarys
library(lubridate)

# Load settings.r
source("settings.r", echo = T)
```

# Get raw data
```{r}
internet_raw <- import(bhrc_noidraw, setclass = "tibble") %>% 
  filter(redcap_event_name != "screening_arm_1") %>% 
  select(ident, redcap_event_name, contains("fat21"), contains("icms"), -contains("complete"))

internet_raw %>% names
```

# Compute
```{r}
internet <- internet_raw %>% 
# Screen time
  ## W0
  set_na(fat21, fat21, fat21a_1, nfat21, nfat21a_1, p_nfat21, p_nfat21a_ns, n2fat21a, contains("icms"), na =99) %>% 
  mutate(fat21a_na = if_else(is.na(fat21a_hr) & is.na(fat21a_min), 1L, 0L), # dom wave 0
         fat21a_hr = replace_na(fat21a_hr, value = 0),
         fat21a_min = replace_na(fat21a_min, value = 0),
         fat21a = str_c(fat21a_hr, fat21a_min, sep = ":") %>% str_remove("Minutes"),
         fat21a = if_else(fat21a_na == 1, NA_character_, fat21a),
         fat21a_1 = if_else(fat21 == 0, 0L, fat21a_1), # dom wave 0
         fat21a = if_else(fat21a_1 == 0, "00:00", fat21a, fat21a),
         #fat21a_1 = if_else(fat21==0, 0L, fat21a_1, fat21a_1),
         #fat21a_hr = if_else(fat21==0 | fat21a_1 %in% c(0,1), 0L, fat21a_hr, fat21a_hr),
         #fat21a_min = str_remove(fat21a_min, "Minutes") %>% as.integer %>% if_else(fat21==0, 0L, .),
         #fat21a = str_c(fat21a_hr, fat21a_min, sep = ":")
  # W1
  ## Dom
         nfat21a_1 = if_else(nfat21 == 0, 0L, nfat21a_1),
         #nfat21a = if_else(nfat21 == 0 | nfat21a_1 == 0, "0:00", nfat21a),
  ## Psico
         p_nfat21a_ns = if_else(p_nfat21 == 0, 0L, p_nfat21a_ns),  # dom Wave1
         #p_nfat21a = if_else(p_nfat21 == 0 | p_nfat21a_ns == 0, "0:00", p_nfat21a), # psi Wave1
  # W2
         screentime_w2 = case_when(n2fat21a == 0 ~ "00:00",  # dom
                                   TRUE ~ n2fat21b),
  ) %>% 
  rename(p_screen = p_nfat21,
         p_screentime = p_nfat21a) %>% 
  # Unite waves
  unite(screen, fat21, nfat21, sep = "", na.rm=T) %>%
  unite(screentime, fat21a, nfat21a, screentime_w2, sep = "", na.rm=T) %>%
  unite(screentime_1, fat21a_1, nfat21a_1, sep = "", na.rm=T) %>%
  # Settings
  mutate(across(starts_with("screen"), ~na_if(., "")),
         across(screentime, ~str_sub(., 1, 5) %>% parse_date_time("H:M") %>% as.character("%H:%M")),
         across(screen, as.numeric)) %>% 
  select(-fat21a_hr, -fat21a_min) %>% 
  # set Var/val labels
  copy_labels(internet_raw) %>%
  var_labels(screen = "Is there a television, computer or video game where the child lives?",
             screentime = "IF yes, how many hours a day, adding everything, does the [name of the child] pass in front of the TV, using the computer and playing video games on average?",
             screentime_1 = "If yes, how many hours a day on average does the child pass in front of the TV, using the computer and playing video games? (ns)")

# summaries  
internet %>% wave(2) %>% sumfac(matches("screen"))
internet %>% names()
internet %>% sumfac()
```

# Export
```{r}
internet %>% export("bhrc_data/Proc/ICMS.rds")
```

