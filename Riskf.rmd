---
title: "Risk_factors"
author: "André Simioni"
date: "29/01/2020"
output: html_document
---
# Librarys and settings
First, read the README.md section about `settings.r` file.
```{r lib,  include=FALSE}
# Aditional librarys

# Load settings.r
source("settings.r", echo = T)
```

# Get raw data
```{r}
riskf_raw <- import(bhrc_noidraw, setclass = "tibble") %>% 
  filter(redcap_event_name != "screening_arm_1") %>% 
  select(ident, redcap_event_name, 
         fat1:fat20b_1, fat24:fat33d, -starts_with("fes"), # Wave 0
         -ends_with("complete"))
riskf_raw %>% names()
riskf_raw$fat28
```

# Compute
Subdivision:
- Education events: fat24:fat27

```{r}
riskf <- riskf_raw %>% 
  rename(gestage = fat10,
         birthweight = fat5,
         # new
         ghypert = fat16a,
         gdm = fat16b,
         guri = fat16c,
         ghosp = fat16d,
         gstress = fat17) %>% 
  set_na(gestage, birthweight, ghypert:gstress, fat28a_1:fat29a, na = 99) %>% 
  # Gestational age and birthweight- vars only colected at wave0
  mutate(gestagec = rec(gestage, rec = "lo:33=0 [Extremely preterm (<34)];
                                        34:36=1 [Moderate preterm (34-36)];
                                        37=2 [Preterm (37)];
                                        38=3 [Early term (38)];
                                        39:41=4 [Term (39-41)];
                                        42:hi=5 [Posterm (>42)];
                                        else=NA"),
         prematurity = case_when(gestage < 37 | fat9 == 1 ~ 0,
                                 gestage >= 37 | fat9 == 2 ~ 1),
         birthweightc = rec(birthweight, rec = "lo:2499=0 [Low weight (<2.5Kg)];
                                                2500:2999=1 [Underweight at birth (2.5-3Kg)];
                                                3000:3499=2 [Adequate birth weight (3-3.5Kg)];
                                                3500:hi=3 [Overweight at birth (>3.5kg)];
                                                else=NA"),
  # Apgar
         apgar1mc = rec(fat7, rec = "7:10=2 [normal]; 4:6=1 [intermediate]; 0:3=0 [poor]; else=NA"),
         apgar5mc = rec(fat7a, rec = "7:10=2 [normal]; 4:6=1 [intermediate]; 0:3=0 [poor]; else=NA"),
  # Prenatal consultation
         prenatalconsultsc = rec(fat8a, rec = "lo:5=0 [poor (<5)]; 6:hi=1 [recommended (>6)]; else=NA"),
  # Gestational Smoke
         gsmoke_ever = rec(fat13, rec = "0=0 [Not exposed]; 1:4=1 [Exposed]; else=NA"),
         gsmoke_levw = case_when(fat13 == 0 ~ 0,
                                 fat13 == 1 ~ NA_real_,
                                 fat13 == 2 ~ as.numeric(fat13a),
                                 fat13 == 3 | fat13 == 4 ~ as.numeric(fat13b)*7),
         gsmoke_levd = gsmoke_levw/7,
         gsmoke_levc = case_when(fat13 == 0 ~ 0,                              # Classified as Victoria(2015)
                                 fat13 == 1 ~ 1,
                                 gsmoke_levd >= 1 & gsmoke_levd <= 14 ~ 2,
                                 gsmoke_levd >14 ~ 3),
         gsmoke_exp = case_when(gsmoke_ever == 0 & fat14 == 0 ~ 0,  # Active/passive exposure
                                gsmoke_ever == 1 & fat14 == 0 ~ 2,
                                gsmoke_ever == 0 & fat14 == 1 ~ 1,
                                gsmoke_ever == 1 & fat14 == 1 ~ 3),
         gsmoke_1tri = case_when(gsmoke_ever == 0 ~ 0,           # 1st trimester
                                 gsmoke_ever == 1 & fat13c1 == 0 ~ 1,
                                 gsmoke_ever == 1 & fat13c1 == 1 ~ 2),
  # Gestational drinking
         gdrink_ever = rec(fat15, rec = "0=0 [Not exposed]; 1:4=1 [Exposed]; else=NA"),
         gdrink_levw = case_when(fat15 == 0 ~ 0,
                                 fat15 == 1 ~ NA_real_,
                                 fat15 == 2 ~ as.numeric(fat15a),
                                 fat15 == 3 | fat15 == 4 ~ as.numeric(fat15b)*7),
         gdrink_levd = gdrink_levw/7,
         gdrink_levc = case_when(fat15 == 0 ~ 0,                             # Classification
                                 fat15 == 1 ~ 1,
                                 gdrink_levw >= 1 & gdrink_levw<=3 ~ 2,
                                 gdrink_levw>3 ~ 3),
  # Breastfeeding
         breastf_lev = case_when(fat20 == 0 ~ 0,
                                 fat20 == 1 & fat20a_y >= 1 & fat20a_m >= 1 ~ fat20a_y*12 + fat20a_m %>% as.numeric,
                                 fat20 == 1 & is.na(fat20a_y) & fat20a_m >= 1 ~ fat20a_m %>% as.numeric,
                                 fat20 == 1 & fat20a_y >= 1 & is.na(fat20a_m) ~ fat20a_y*12 %>% as.numeric,
                                 fat20a_1 == 0 ~ 0.5),
         breastf_levc = case_when(breastf_lev == 0 ~ 0,                              # Classified as Victoria(2015)
                                  redcap_event_name == "wave0_arm_1" & breastf_lev == NA ~ 1,
                                  breastf_lev >0 & breastf_lev < 1 ~ 2,
                                  breastf_lev >= 1 & breastf_lev < 3 ~ 3,
                                  breastf_lev >= 3 & breastf_lev < 6 ~ 4,
                                  breastf_lev >= 6 & breastf_lev < 12 ~ 5,
                                  breastf_lev >= 12 ~ 6),
  ## Exclusive breasteeding (Victora 2015)
        breastf_exc_lev = case_when(fat20 == 0 ~ 0,
                                    fat20 == 1 & fat20b_y >= 1 & fat20b_m >= 1 ~ fat20b_y*12 + fat20b_m %>% as.numeric,
                                    fat20 == 1 & fat20b_y == NA & fat20b_m >= 1 ~ fat20b_m %>% as.numeric,
                                    fat20 == 1 & fat20b_y >= 1 & fat20b_y == NA ~ fat20b_y*12 %>% as.numeric,
                                    fat20a_1 == 0 ~ 0.5),
  ## Exclusive breasteeding classification (Victora 2015)
        breastf_exc_c = case_when(breastf_exc_lev == 0 ~ 0,
                                  redcap_event_name == "wave0_arm_1" & breastf_exc_lev == NA ~ 1,
                                  breastf_exc_lev >0 & breastf_exc_lev < 1 ~ 2,
                                  breastf_exc_lev  >=  1 & breastf_exc_lev < 3 ~ 3,
                                  breastf_exc_lev >= 3 & breastf_exc_lev < 6 ~ 4,
                                  breastf_exc_lev >= 6 & breastf_exc_lev < 12 ~ 5,
                                  breastf_exc_lev >= 12 ~ 6))


select(riskf, ghypert:gstress) %>% sumfac()
riskf$gdrink_ever %>% summary.factor()
riskf %>% names()
riskf %>% sumfac()

risk_labels <- riskf %>% 
  # var/val labels
  set_labels(gstress, labels = c(None=0, Minimum=1, Light=2, Light=3, Moderate=4, Moderate=5, Moderate=6, Intense=7, Intense=8, "Very intense"=9, Maximum=10)) %>% 
  # new
  set_labels(prematurity, labels = c(Preterm=0, 'Term/Posterm'=1)) %>% 
  set_labels(gsmoke_levc, labels = c("Not Exposed"=0, "Minimal exposure (not periodically)"=1, "Some Exposure (1-14 cig/day)"=2, "Heavy exposure (>14 cig/day)"=3)) %>% 
  set_labels(gsmoke_exp, labels = c("Not exposed"=0, "Passive exposue"=1, "Active exposure"=2, "Active/passive exposue"=3)) %>% 
  set_labels(gsmoke_1tri, labels = c("Never smoked"=0, "Smoked but not on 1st tri"=1, "Smoked on 1st tri"=2)) %>% 
  set_labels(gdrink_levc, labels = c("Not exposed"=0, "Minimal exposure (not periodically)"=1, "Some Exposure (1-3 drinks/week)"=2, "Heavy exposure (>3 drinks/week)"=3)) %>% 
  set_labels(breastf_levc, breastf_exc_c, labels = c(No=0, "Yes (undefined)"=1, "<1 month"=2, "1-2.9 months"=3, "3-5.9 months"=4, "6-11.9 months"=5, ">12 months"=6 )) %>% 
  # Variable labels
  var_labels(gestage = "Gestational age in weeks",
             gestagec = "Gestational age classif.",
             prematurity = "Prematurity",
             birthweight = "Birthweight in grams",
             birthweightc = "Birthweight classif.",
             apgar1mc = "Apgar 1m classif.",
             apgar5mc = "Apgar 5m classif.",
             prenatalconsultsc = "Nº prenatal consults classif.",
             gsmoke_ever = "Ever smoked during pregnancy at W0",
             gsmoke_levw = "Smoke during pregnancy: cig/week at W0",
             gsmoke_levd = "Smoke during pregnancy: cig/day at W0",
             gsmoke_levc = "Smoke during pregnancy: Level of exposure at W0",
             gsmoke_exp = "Act/Passive smoking exposure at W0",
             gsmoke_1tri = "Smked in 1 trimester",
             gdrink_ever = "Ever drinked during pregnancy at W0",
             gdrink_levw = "Drink during pregnancy: doses/week at W0",
             gdrink_levd = "Drink during pregnancy: doses/day at W0",
             gdrink_levc = "Drinking during pregnancy: Level of exposure at W0",
             breastf_lev = "Breastfeed level in months",
             breastf_levc = "Breastfeed classification",
             breastf_exc_lev = "Exclusive breastfeed level in months",
             breastf_exc_c = "Exclusive breastfeed classif.",
             gstress = "Stress level experienced during pregnancy"
             )
riskf %>% names()
```

# Export
```{r}
risk_labels %>% export("~/Documentos/pCloud/Projetos/Analises/bhrc/bhrc_data/Proc/Riskf_new.rds")
#risk_labels %>% select(subjectid, redcap_event_name, matches("screen"), matches("fat21")) %>% export("screen.rds")
```
