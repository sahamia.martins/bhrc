---
title: "HRC - Family Environment Scale (FES)"
author: "André Simioni"
date: "15 de agosto de 2017"
output: html_document
editor_options: 
  chunk_output_type: console
---
# Librarys and settings
First, read the README.md section about `settings.r` file.
```{r lib,  include=FALSE}
# Aditional librarys
library(mice); library(lavaan); library(broom)

# Load settings.r
source("settings.r", echo = T)
```

# Get data from redcap
## W0W1
```{r}
fes_w0bi <- import(bhrc_noidraw, setclass = "tibble") %>% 
  select(ident, redcap_event_name, fes_qol1:fes53r) %>% 
  filter(redcap_event_name == "wave0_arm_1") %>% 
  set_na(fes_qol1, na=99) %>% 
  rec(fes6r, fes7, fes11, fes17, fes36, fes38, fes41, fes50, fes53r, rec = "0=1; 1=0; else=copy") %>% 
  mutate(across(starts_with("fes"), ordered))
#fes_w0bi %>% sumfac()
```

# Impute data (mice package)
```{r}
## Setting the parameters
init <- mice(fes_w0bi, maxit=0)
meth <- init$method # which methods will be used
predM <- init$predictorMatrix

## Imputing
fes_w0bi %>% map_dbl(~sum(is.na(.))/length(.)) # % of missings before imputation
fes_w0i <- mice(fes_w0bi, method = meth, predictorMatrix = predM, m = 1, maxit = 1, seed=500) %>% 
  complete() %>% 
  as_tibble() %>% 
  mutate_at(vars(fes_qol1:fes53r_r), ordered)
fes_w0i %>% map_dbl(~sum(is.na(.))/length(.)) # % of missings after imputation
fes_w0i %>% summary()
```

# Latent variables
```{r}
# Unidimensional factors
model <- "# Defining latent variables
fes_cohe_fl =~ fes1 + fes7_r + fes12 + fes18 + fes24r + fes30 + fes36_r + fes42 + fes48
"
cohe_fit <- cfa(model, data = fes_w0i, estimator="WLSMV", std.lv=T)
cohe_fit %>% glance()

model<-"# Defining latent variables
fes_confl_fl =~ fes3 + fes9r + fes14 + fes20r + fes26 + fes32 + fes38_r + fes44 + fes50_r + fes53r_r
"
confl_fit <- cfa(model,data=fes_w0i,estimator="WLSMV",std.lv=T)
confl_fit %>% glance()

model<-"# Defining latent variables
fes_ctrl_fl =~ fes6r_r + fes11_r + fes17_r + fes23 + fes29 + fes35 + fes41_r + fes47
"
ctrl_fit <- cfa(model,data=fes_w0i,estimator="WLSMV",std.lv=T)
ctrl_fit %>% glance()

# Correlated model
model <- "# Defining latent variables
fes_cohe_fl =~ fes1 + fes7_r + fes12 + fes18 + fes24r + fes30 + fes36_r + fes42 + fes48
fes_confl_fl =~ fes3 + fes9r + fes14 + fes20r + fes26 + fes32 + fes38_r + fes44 + fes50_r + fes53r_r
fes_ctrl_fl =~ fes6r_r + fes11_r + fes17_r + fes23 + fes29 + fes35 + fes41_r + fes47
"
cor_fit <- cfa(model, data = fes_w0i, estimator="WLSMV", std.lv=T)
#cor_fit %>% modindices()

# Fit
lst(cohe_fit, confl_fit, ctrl_fit, cor_fit) %>% map_df(glance, .id="id")
cor_fit %>% tidy() %>% print(n=30)

cor_fit %>% semPlot::semPaths(what="paths", intercepts=F, residuals=F, sizeLat = 6, sizeMan = 4, sizeMan2=4) # Diagram
cor_fit %>% semTools::reliability() # Reliability from the model - omega
cor_pred <- cor_fit %>% lavPredict # Extract factor loadings
```

# Bind cols and summed scores
```{r}
# Bind factor loadings cols
fes <- cbind(fes_w0i, cor_pred) %>% 
  as.tibble() %>% 
  mutate(across(starts_with("fes"), ~as.character(.) %>% as.numeric(.))) %>% 
  row_sums(fes1, fes7_r, fes12, fes18, fes24r, fes30, fes36_r, fes42, fes48, var = "fes_cohe", n=0.90) %>% 
  row_sums(fes3, fes9r, fes14, fes20r, fes26, fes32, fes38_r, fes44, fes50_r, fes53r_r, var = "fes_confl", n=0.90) %>% 
  row_sums(fes6r_r, fes11_r, fes17_r, fes23, fes29, fes35, fes41_r, fes47, var = "fes_ctrl", n=0.90) %>%
  # set var / val labels
  set_labels(fes1:fes53r, labels = c(True=1, False = 0)) %>%
  set_labels(ends_with("_r"), labels = c(True=0, False = 1)) %>% 
  var_labels(fes_cohe="FES: cohesion score",
             fes_confl="FES: conflict score",
             fes_ctrl="FES: control score",
             fes6r_r="FES6 unrecoded",
             fes7_r="FES7 recoded",
             fes11_r="FES11 recoded",
             fes17_r="FES17 recoded",
             fes36_r="FES36 recoded",
             fes38_r="FES38 recoded",
             fes41_r="FES41 recoded",
             fes50_r="FES50 recoded",
             fes53r_r="FES53r unrecoded",
             fes_cohe_fl="FES: Cohesion factor loading (correlated mod)",
             fes_ctrl_fl="FES: Control factor loading  (correlated mod)",
             fes_confl_fl="FES: Conflict factor loading (correlated mod)")
```

# Export
```{r eval=FALSE, include=FALSE}
fes %>% export("bhrc_data/Raw/Data/Proc/FES.rds")
```

