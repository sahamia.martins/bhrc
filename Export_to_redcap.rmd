---
title: "Export Data to Redcap"
author: "André Simioni"
date: "17/01/2020"
output: html_document
editor_options: 
  chunk_output_type: console
---
# Librarys and settings
First, read the README.md section about `settings.r` file.
```{r lib,  include=FALSE}
# Aditional librarys
library(skimr)

# Load settings.r
setwd("/home/andresimi/Documentos/pCloud/Projetos/Analises/bhrc")
source("settings.r", echo = T)
```

# Open Data
```{r}
#subjectid <- import("bhrc_data/Proc/subjectid.rds", setclass = "tibble")

redcap <- import("bhrc_data/Proc/Neuroimage.rds", setclass = "tibble") %>% select(ident, redcap_event_name, 211:last_col())
redcap %>% names()
redcap$sh15 %>% get_label()
#redcap %>% wave(2) %>% sumfac(ends_with("_fl"))

#redcap %>% skim()

#redcap <- redcap %>% left_join(subjectid) %>% mutate(redcap_event_name = "wave2_covid_arm_1") %>% select(ident, redcap_event_name, dq=DQ) %>% arrange(ident)
```

# Build data Dictionary
Redcap data dictionary headers:   
> Variable / Field Name	Form Name	Section Header	Field Type	Field Label	Choices, Calculations, OR Slider Labels	Field Note

Example of Redcap label: 
> 0, Nothing | 1, A little | 2, Much | 3, Extremely

```{r}
# function to transcode get_labels to Redcap label
to_redcap_label <- function(x) x %>% get_labels(values = "p") %>% paste(collapse = " | ") %>% str_remove_all(., "\\[") %>% str_replace_all("\\]", ",")

# Data Dictionary
dic <- tibble(field_name = redcap %>% names,
              form_name = "neuroimage_w0w2",
              section_header = NA_character_,
              field_type = "text", # text / dropdown
              field_label = redcap %>% get_label,
              choices = redcap %>% map_chr(to_redcap_label),
              field_note = "",
              validation = "number", # number / NULL
              validation_min = "",
              validation_max = "")
dic %>% print(n=100)
#dic$choices
```

# Check redcap
```{r}
check <- redcap %>% filter(is.na(ident))
check
check$redcap_event_name %>% summary.factor()

redcap <- redcap %>% drop_na(ident)

#exp <- redcap %>% select(ident, redcap_event_name, 1001:1165)
```

# Export
```{r}
dic %>% export("bhrc_data/Export/Export_data/redcap_export_dic.csv")

redcap %>% export("bhrc_data/Export/Export_data/redcap_export_data.csv")
#file.remove("bhrc_data/Export/Export_data/redcap_export_dic.csv")
#file.remove("bhrc_data/Export/Export_data/redcap_export_data.csv")

redcap %>% length()
redcap %>% names()
```
1- download redcap data dictionary and add the export_dic.csv to the end of document
2- designate the instrument to the specified redcap events
3- upload the updated redcap data dictionary
4- upload the export_data.csv
