---
title: "TAP"
author: "André Rafael Simioni"
date: "25/02/2021"
output: html_document
editor_options: 
  chunk_output_type: console
---

# Librarys and settings
First, read the README.md section about `settings.r` file.
```{r lib,  include=FALSE}
# Aditional librarys

# Load settings.r
setwd("~/Documentos/pCloud/Projetos/Analises/bhrc/")
source("settings.r", echo = T)
```

# Open data
```{r}
tap_raw <- import(bhrc_noidraw, setclass = "tibble") %>%
  select(ident, redcap_event_name, starts_with("tap")) %>%
  filter(redcap_event_name == "wave2_arm_1")

tap_raw$tap1 %>% get_labels(values = "p")
```

# Recode data
```{r}
tap <- tap_raw %>%
  rec(starts_with("tap"), rec = "2=0;else=copy", suffix = "") %>%
  # set error if 10 consecutive errors
  mutate(tap10 = if_else(tap0==0 & tap1==0 & tap2==0 & tap3==0 & tap4==0 & tap5==0 & tap6==0 & tap7==0 & tap8==0 & tap9==0, 0, tap10),
         tap11 = if_else(tap1==0 & tap2==0 & tap3==0 & tap4==0 & tap5==0 & tap6==0 & tap7==0 & tap8==0 & tap9==0 & tap10==0, 0, tap11),
         tap12 = if_else(tap2==0 & tap3==0 & tap4==0 & tap5==0 & tap6==0 & tap7==0 & tap8==0 & tap9==0 & tap10==0 & tap11==0, 0, tap12),
         tap13 = if_else(tap3==0 & tap4==0 & tap5==0 & tap6==0 & tap7==0 & tap8==0 & tap9==0 & tap10==0 & tap11==0 & tap12==0, 0, tap13),
         tap14 = if_else(tap4==0 & tap5==0 & tap6==0 & tap7==0 & tap8==0 & tap9==0 & tap10==0 & tap11==0 & tap12==0 & tap13==0, 0, tap14),
         tap15 = if_else(tap5==0 & tap6==0 & tap7==0 & tap8==0 & tap9==0 & tap10==0 & tap11==0 & tap12==0 & tap13==0 & tap14==0, 0, tap15),
         tap16 = if_else(tap6==0 & tap7==0 & tap8==0 & tap9==0 & tap10==0 & tap11==0 & tap12==0 & tap13==0 & tap14==0 & tap15==0, 0, tap16),
         tap17 = if_else(tap7==0 & tap8==0 & tap9==0 & tap10==0 & tap11==0 & tap12==0 & tap13==0 & tap14==0 & tap15==0 & tap16==0, 0, tap17),
         tap18 = if_else(tap8==0 & tap9==0 & tap10==0 & tap11==0 & tap12==0 & tap13==0 & tap14==0 & tap15==0 & tap16==0 & tap17==0, 0, tap18),
         tap19 = if_else(tap9==0 & tap10==0 & tap11==0 & tap12==0 & tap13==0 & tap14==0 & tap15==0 & tap16==0 & tap17==0 & tap18==0, 0, tap19),
         tap20 = if_else(tap10==0 & tap11==0 & tap12==0 & tap13==0 & tap14==0 & tap15==0 & tap16==0 & tap17==0 & tap18==0 & tap19==0, 0, tap20),
         tap21 = if_else(tap11==0 & tap12==0 & tap13==0 & tap14==0 & tap15==0 & tap16==0 & tap17==0 & tap18==0 & tap19==0 & tap20==0, 0, tap21),
         tap22 = if_else(tap12==0 & tap13==0 & tap14==0 & tap15==0 & tap16==0 & tap17==0 & tap18==0 & tap19==0 & tap20==0 & tap21==0, 0, tap22),
         tap23 = if_else(tap13==0 & tap14==0 & tap15==0 & tap16==0 & tap17==0 & tap18==0 & tap19==0 & tap20==0 & tap21==0 & tap22==0, 0, tap23),
         tap24 = if_else(tap14==0 & tap15==0 & tap16==0 & tap17==0 & tap18==0 & tap19==0 & tap20==0 & tap21==0 & tap22==0 & tap23==0, 0, tap24),
         tap25 = if_else(tap15==0 & tap16==0 & tap17==0 & tap18==0 & tap19==0 & tap20==0 & tap21==0 & tap22==0 & tap23==0 & tap24==0, 0, tap25),
         tap26 = if_else(tap16==0 & tap17==0 & tap18==0 & tap19==0 & tap20==0 & tap21==0 & tap22==0 & tap23==0 & tap24==0 & tap25==0, 0, tap26),
         tap27 = if_else(tap17==0 & tap18==0 & tap19==0 & tap20==0 & tap21==0 & tap22==0 & tap23==0 & tap24==0 & tap25==0 & tap26==0, 0, tap27),
         tap28 = if_else(tap18==0 & tap19==0 & tap20==0 & tap21==0 & tap22==0 & tap23==0 & tap24==0 & tap25==0 & tap26==0 & tap27==0, 0, tap28),
         tap29 = if_else(tap19==0 & tap20==0 & tap21==0 & tap22==0 & tap23==0 & tap24==0 & tap25==0 & tap26==0 & tap27==0 & tap28==0, 0, tap29),
         tap30 = if_else(tap20==0 & tap21==0 & tap22==0 & tap23==0 & tap24==0 & tap25==0 & tap26==0 & tap27==0 & tap28==0 & tap29==0, 0, tap30),
         tap31 = if_else(tap21==0 & tap22==0 & tap23==0 & tap24==0 & tap25==0 & tap26==0 & tap27==0 & tap28==0 & tap29==0 & tap30==0, 0, tap31),
         tap32 = if_else(tap22==0 & tap23==0 & tap24==0 & tap25==0 & tap26==0 & tap27==0 & tap28==0 & tap29==0 & tap30==0 & tap31==0, 0, tap32),
         tap33 = if_else(tap23==0 & tap24==0 & tap25==0 & tap26==0 & tap27==0 & tap28==0 & tap29==0 & tap30==0 & tap31==0 & tap32==0, 0, tap33),
         tap34 = if_else(tap24==0 & tap25==0 & tap26==0 & tap27==0 & tap28==0 & tap29==0 & tap30==0 & tap31==0 & tap32==0 & tap33==0, 0, tap34),
         tap35 = if_else(tap25==0 & tap26==0 & tap27==0 & tap28==0 & tap29==0 & tap30==0 & tap31==0 & tap32==0 & tap33==0 & tap34==0, 0, tap35),
         tap36 = if_else(tap26==0 & tap27==0 & tap28==0 & tap29==0 & tap30==0 & tap31==0 & tap32==0 & tap33==0 & tap34==0 & tap35==0, 0, tap36),
         tap37 = if_else(tap27==0 & tap28==0 & tap29==0 & tap30==0 & tap31==0 & tap32==0 & tap33==0 & tap34==0 & tap35==0 & tap36==0, 0, tap37),
         tap38 = if_else(tap28==0 & tap29==0 & tap30==0 & tap31==0 & tap32==0 & tap33==0 & tap34==0 & tap35==0 & tap36==0 & tap37==0, 0, tap38),
         tap39 = if_else(tap29==0 & tap30==0 & tap31==0 & tap32==0 & tap33==0 & tap34==0 & tap35==0 & tap36==0 & tap37==0 & tap38==0, 0, tap39),
         tap40 = if_else(tap30==0 & tap31==0 & tap32==0 & tap33==0 & tap34==0 & tap35==0 & tap36==0 & tap37==0 & tap38==0 & tap39==0, 0, tap40)) %>%
  # var/val labels
  copy_labels(tap_raw) %>%
  set_labels(starts_with("tap"), labels = c(Wrong=0, Right=1))

# summary
tap %>% sumfac(-1, -2, .label = F)
```

# Export
```{r}
tap %>% export("bhrc_data/Proc/TAP.rds")
```

