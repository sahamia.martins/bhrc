# Open standard libraries
library(rio); library(rlang); library(tidyverse); library(sjmisc); library(sjlabelled);

# Setting working directory
setwd("~/Documentos/pCloud/Projetos/Analises/bhrc") # Andre
bhrc_noidraw <- "bhrc_raw/bhrc_noidraw_v0.6_lng.rds"

# Load a summary factor formula
sumfac <- function(df, ..., .label = F, .max = 6) {
  # setting
  if (...length() ==0) dots <- tidyselect::vars_select(names(df), everything())
  else dots <- tidyselect::vars_select(names(df), ...)
  
  # formula
  if (.label == F) select(df, all_of(dots)) %>% sjlabelled::as_factor() %>% summary(maxsum = .max) 
  else select(df, all_of(dots)) %>% sjlabelled::as_label(prefix=T) %>% summary(maxsum = .max)
}

# Filter wave function
wave <- function(df, wave) {
  wave <- case_when(wave == -1 ~ "screening_arm_1",
                    wave == 0 ~ "wave0_arm_1",
                    wave == 1 ~ "wave1_arm_1",
                    wave == 2 ~ "wave2_arm_1",
                    wave == "2c" ~ c("wave2_covid1_arm_1", "wave2_covid2_arm_1"))
  df %>% filter(redcap_event_name %in% wave)
}


