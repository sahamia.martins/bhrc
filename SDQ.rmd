---
title: "SDQ"
author: "André Simioni"
date: "01/07/2021"
output: html_document
editor_options: 
  chunk_output_type: console
---
# Librarys and settings
First, read the README.md section about `settings.r` file.
```{r lib,  include=FALSE}
# Aditional libraries


# Load settings.r
source("settings.r", echo = T)
```

Dependencies
dawbanet_clean.rmd
covid1.rmd
covid2.rmd

# Open Data
```{r}
sdq_raw <- import("bhrc_data/Proc/dawbanet_df.rds", setclass = "tibble") %>% 
  select(ident, redcap_event_name, p1consid:p1impact, sconsid:simpact, t1consid:t1impact)

covid_sdq <- import("bhrc_data/Proc/Intermediate/covid_sdq.rds", setclass = "tibble")

lst(sdq_raw, covid_sdq) %>% map(names)
```

# Bind rows
```{r}
sdq <- bind_rows(sdq_raw, covid_sdq) %>% 
  arrange(ident, redcap_event_name)
```

# Export
```{r}
sdq %>% export("bhrc_data/Proc/SDQ.rds")
```

